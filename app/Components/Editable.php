<?php
declare(strict_types=1);

namespace App\Components;

use App\Models\Image;
use Spatie\TranslationLoader\LanguageLine;

class Editable
{
    private static self $instance;

    private function __construct()
    {
    }

    public static function instance(): self
    {

        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function create(string $tagName, ?string $editUrl, array $options = [], ?string $content = null, ?bool $closeTag = true): string
    {
        $options['data-edit-url'] = $editUrl;

        if (\Auth::check()) {
            $options['title'] = $options['title'] ?? trans('static.editable_text');
            $options['class'] = ($options['class'] ?? '') . ' editable';
        }

        $string = sprintf('<%s%s>', $tagName, self::generateHtmlOptionsToString($options));

        if ($closeTag) {
            if ($content) {
                $string .= sprintf('%s', $content);
            }
            $string .= sprintf('</%s>', $tagName);
        }

        return $string;
    }

    public function img(string $param, ?string $alt = null, array $options = []): string
    {
        if ($image = $this->getImage($param)) {
            $options['src'] = get_image($image->filename, $image->path);
        }

        $url = $image ? route('admin.images.edit', $image->id) : route('admin.images.create', ['param' => $param]);


        if ($alt) {
            $options['alt'] = $alt;
        }

        if (\Auth::check()) {
            $options['class'] = ($options['class'] ?? '') . ' editable-image';
        }

        return $this->create('img', $url ?? null, $options, null, false);
    }

    public function bg(string $param, array $options = []): string
    {
        if ($image = $this->getImage($param)) {
            $src = get_image($image->filename, $image->path);
            $options['style'] = "background-image:url('$src');" . ($options['style'] ?? '');
        }
        $url = $image ? route('admin.images.edit', $image->id) : route('admin.images.create', ['param' => $param]);

        if (\Auth::check()) {
            $options['class'] = ($options['class'] ?? '') . ' editable-image';
        }

        return $this->create('div', $url ?? null, $options);
    }

    public function text(string $param, ?array $values = [], array $options = []): string
    {
        if (\Auth::check()) {
            $url = ($text = $this->getText($param)) ? route('admin.translates.edit', $text->id) : route('admin.translates.create', ['param' => $param]);
            $options['class'] = ($options['class'] ?? '') . ' editable-text';
        } else {
            return trans($param, $values);
        }

        return $this->create('span', $url ?? null, $options, trans($param, $values));
    }

    public function src(string $src): string
    {
        return $src;
    }

    protected static function generateHtmlOptionsToString(array $options): string
    {
        $htmlOptionsAsString = join(' ', array_map(function ($key) use ($options) {
            if (is_bool($options[$key])) {
                return $options[$key] ? $key : '';
            }
            return $key . '="' . $options[$key] . '"';
        }, array_keys($options)));
        if (!empty($htmlOptionsAsString)) {
            $htmlOptionsAsString = ' ' . $htmlOptionsAsString;
        }
        return $htmlOptionsAsString;
    }

    public function getImage($param)
    {
        return Image::query()
            ->select('p.path', 'p.filename', 'images.id as id')
            ->join('photos as p', 'p.id', '=', 'images.photo_id')
            ->where('param', $param)
            ->first();
    }

    public function getText($param)
    {
        $params = explode('.', $param, 2);
        $translate = LanguageLine::query()
            ->where('group', $params[0]);

        if (isset($params[1])) {
            $translate->where('key', $params[1]);
        }

        return $translate->first();
    }
}
