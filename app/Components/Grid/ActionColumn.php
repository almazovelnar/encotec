<?php

namespace App\Components\Grid;

use Itstructure\GridView\Actions\Delete;

class ActionColumn extends \Itstructure\GridView\Columns\ActionColumn
{
    public function render($row)
    {
        $value = '';
        $bootstrapColWidth = floor(12 / count($this->actionObjects));
        foreach ($this->actionObjects as $actionObj) {
            if (($actionObj instanceof Delete) && !\Auth::user()->hasRole('admin')) {
                continue;
            }

            $value .= $actionObj->render($row, $bootstrapColWidth);
        }

        return '<div class="row">' . $value . '</div>';
    }
}
