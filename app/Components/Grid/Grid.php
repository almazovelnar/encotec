<?php

namespace App\Components\Grid;

use Illuminate\Pagination\LengthAwarePaginator;
use function view;

class Grid extends \Itstructure\GridView\Grid
{
    protected $tableBordered = false;
    protected $tableStriped = true;
    protected $tableHover = false;
    protected $tableSmall = false;
    protected $useFilterSearchButton = false;
    protected $useFilterResetButton = false;
    protected $hideCreateButton = false;
    protected $createUrl = null;
    protected $langSelect = null;
    protected $langCreate = null;
    protected $dataInfo = true;
    protected $gridHeader = null;

    /**
     * @return string
     */
    public function render(): string
    {
        $this->applyColumnsConfig();

        $this->dataProvider->selectionConditions($this->request, $this->strictFilters);

        $totalCount = $this->dataProvider->getCount();
        $pageNumber = $this->request->get($this->paginatorOptions['pageName'] ?? 'page', $this->page);
        $items = $this->dataProvider->get($this->rowsPerPage, $pageNumber);

        if ($items->count() < $this->rowsPerPage && $totalCount > 0) {
            $totalCount = $this->rowsPerPage * $pageNumber;
        }

        $this->paginator = new LengthAwarePaginator(
            $items,
            $totalCount,
            $this->rowsPerPage,
            $pageNumber,
            $this->paginatorOptions
        );

        return view('grid_view::grid', [
            'columnObjects'         => $this->columnObjects,
            'useFilters'            => $this->useFilters,
            'paginator'             => $this->paginator,
            'title'                 => $this->title,
            'rowsFormAction'        => $this->rowsFormAction,
            'useSendButtonAnyway'   => $this->useSendButtonAnyway,
            'searchButtonLabel'     => $this->getSearchButtonLabel(),
            'resetButtonLabel'      => $this->getResetButtonLabel(),
            'sendButtonLabel'       => $this->getSendButtonLabel(),
            'tableBordered'         => $this->tableBordered,
            'tableStriped'          => $this->tableStriped,
            'tableHover'            => $this->tableHover,
            'tableSmall'            => $this->tableSmall,
            'hideCreateButton'      => $this->hideCreateButton,
            'useFilterSearchButton' => $this->useFilterSearchButton,
            'useFilterResetButton'  => $this->useFilterResetButton,
            'createUrl'             => $this->createUrl,
            'langSelect'            => $this->langSelect,
            'langCreate'            => $this->langCreate,
            'dataInfo'              => $this->dataInfo,
            'gridHeader'            => $this->gridHeader,
        ])->render();
    }
}

