<?php
namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac:create-roles';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Role::query()->whereNotIn('name', $this->getRoles())->delete();
        Permission::query()->whereRaw('1 > 0')->delete();

        foreach ($this->getRoles() as $role)
            Role::findOrCreate($role);
        foreach ($this->getPermissions() as $permission)
            Permission::findOrCreate($permission);

        foreach (Role::all() as $role){
            if (isset($this->getRules()[$role->name])) {
                $permissions = Permission::whereIn('name', $this->getRules()[$role->name])->pluck('id');
                $role->syncPermissions($permissions);
            }
        }

        User::first()->assignRole(Role::where('name', 'admin')->first());
    }


    private function getRoles()
    {
        return [
            'admin',
            'moderator',
            'videoEditor'
        ];
    }

    private function getPermissions()
    {
        return [
            'categoryManage',
            'channelManage',
            'postManage',
            'cityManage',
            'shortManage',
            'tagManage',
            'pageManage',
            'userManage',
            'authorManage',
            'configManage',
            'translateManage',
            'mainInfoManage',
        ];
    }

    private function getRules()
    {
        return [
            'admin'     => $this->getPermissions(),
            'moderator' => [
                'categoryManage',
                'channelManage',
                'postManage',
                'shortManage',
                'tagManage',
                'pageManage',
                'translateManage'
            ],
            'videoEditor' => [
                'postManage'
            ],
        ];
    }
}
