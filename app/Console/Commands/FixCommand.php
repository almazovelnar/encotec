<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Console\Command;

class FixCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fix {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        switch ($this->argument('name')) {
            case 'category_tree':
                $this->fixCategoryTree();
                break;
            default:
                $this->optimizeApp();
                break;
        }
        return Command::SUCCESS;
    }

    private function optimizeApp()
    {
//        \Artisan::call('insert:views');
        \Artisan::call('optimize:clear');
        \Artisan::call('optimize');
        \Artisan::call('route:trans:cache');

        $this->info('App optimized successfully!');
    }

    private function fixCategoryTree()
    {
        Category::fixTree();
        $this->info('Category tree fixed successfully!');
    }
}
