<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Redis;

class InsertViews extends Command
{
    protected $signature = 'insert:views';
    protected $description = 'Command description';

    public function handle()
    {
        $keyName = 'views_post_';
        /** @var Redis $redis */
        $redis = \App::get('redis');
        $keys = $redis->keys($keyName . '*');
        $prefix = $redis->getOption(Redis::OPT_PREFIX);

        foreach ($keys as $key) {
            preg_match('/^' . $prefix . '(' . $keyName . '(\d+))$/', $key, $matches);
            [$key, $withoutPrefix, $postId] = $matches;
            if (empty($postId)) {
                $redis->del($withoutPrefix);
                continue;
            }

            $viewCount = $redis->get($keyName . $postId);
            \DB::update('UPDATE posts SET views=views+:count WHERE id=:id', [(int)$viewCount, (int)$postId]);
            $redis->del($withoutPrefix);
        }
    }
}
