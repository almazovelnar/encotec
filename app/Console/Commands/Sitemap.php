<?php
namespace App\Console\Commands;

use App\Services\Sitemap\SitemapFactory;
use Illuminate\Console\Command;

class Sitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:index {lang}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        private SitemapFactory $sitemapFactory
    )
    {
        parent::__construct();
    }

    public function handle(): int
    {
        ini_set('memory_limit', '1024M');

        $this->sitemapFactory
            ->createCommonBuilder()
            ->setLocale($this->argument('lang'))
            ->generate();

        return Command::SUCCESS;
    }
}
