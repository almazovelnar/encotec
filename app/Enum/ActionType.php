<?php
declare(strict_types=1);

namespace App\Enum;

class ActionType
{
    const CREATE = 'create';
    const UPDATE = 'update';

    public static function getList(): array
    {
        return [
            self::CREATE => trans('admin.status.deactivated'),
            self::UPDATE   => trans('admin.status.activated')
        ];
    }

    public static function get(int $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
