<?php

namespace App\Enum\ImageTemplates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class LargePhoto implements FilterInterface
{
    const folderName = 'large';

    public function applyFilter(Image $image)
    {
        $code   = request('code');
        $author = request('author');
        $width  = null;
        $height = null;
        if ($image->getHeight() > $image->getWidth()) {
            $height    = 1200;
            $width     = 800;
            $watermark = 'watermarkH.png';

            $codeY   = $height - 204;
            $authorY = $height - 230;
        } else {
            $width     = 1200;
            $height    = 800;
            $watermark = 'watermark.png';

            $codeY   = $height - 204;
            $authorY = $height - 230;
        }

        $codeX     = $width - 280;
        $authorX   = $width - 280;
//        $cachePath = 'media/' . self::folderName;
//
//        file_put_contents(__DIR__ . '/code.'.time().'txt', $image->dirname);
//
//        if (!file_exists(public_path($cachePath))) {
//            if (!file_exists(public_path('media'))) {
//                \File::makeDirectory(public_path('media'));
//            }
//            \File::makeDirectory(public_path($cachePath));
//        }

        $path = storage_path("app/public/photos/posts/" . self::folderName);

        if (!file_exists($path)) {
            \Storage::disk('public')->createDir("photos/posts/" . self::folderName);
        }
        $filename = $image->filename . '.' . $image->extension;

        return $image
            ->resize($width ?? null, $height ?? null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save($path . '/' . $filename, 100);
    }
}
