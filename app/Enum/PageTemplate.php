<?php
declare(strict_types=1);

namespace App\Enum;

class PageTemplate
{
    const MAIN = 'main';
    const SIMPLE = 'simple';

    public static function getList(): array
    {
        return [
            self::SIMPLE  => trans('admin.page_template.simple'),
//            self::MAIN => trans('admin.page_template.main'),
        ];
    }

    public static function get(string $type): string
    {
        if (array_key_exists($type, self::getList())) {
            return self::getList()[$type];
        }

        return '';
    }
}
