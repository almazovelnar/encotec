<?php
declare(strict_types=1);

namespace App\Enum;

class Type
{
    const MAIN = 'main';
    const NORMAL = 'normal';
    const PRIVATE = 'private';
    const SHORT = 'short';

    public static function getList(): array
    {
        return [
            self::NORMAL  => trans('admin.typesList.normal'),
//            self::MAIN    => trans('admin.typesList.main'),
            self::PRIVATE => trans('admin.typesList.private'),
        ];
    }

    public static function get(string $type): string
    {
        if (array_key_exists($type, self::getList())) {
            return self::getList()[$type];
        }

        return '';
    }
}
