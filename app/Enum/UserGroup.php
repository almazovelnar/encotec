<?php
declare(strict_types=1);

namespace App\Enum;

class UserGroup
{
    const PUBLIC = 1;
    const LOCAL = 2;

    public static function getList(): array
    {
        return [
            self::PUBLIC => trans('admin.user_group.public'),
            self::LOCAL => trans('admin.user_group.local'),
        ];
    }

    public static function get(int $groupId): string
    {
        if (array_key_exists($groupId, self::getList())) {
            return self::getList()[$groupId];
        }

        return '';
    }
}
