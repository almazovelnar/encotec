<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Itstructure\GridView\Helpers\SortHelper;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class DataProvider extends EloquentDataProvider
{
    private string $alias;
    private array $enabledFilters;
    private bool $calculateTotalCount;

    public function __construct(
        Builder $query,
        array   $enabledFilters = [],
        string  $mainTableAlias = '',
        bool    $calculateTotalCount = true
    )
    {
        $this->enabledFilters      = $enabledFilters;
        $this->alias               = $mainTableAlias;
        $this->calculateTotalCount = $calculateTotalCount;
        parent::__construct($query);
    }

    public function selectionConditions(Request $request, bool $strictFilters = false): void
    {
        if ($request->get('sort')) {
            $this->query->orderBy(SortHelper::getSortColumn($request), SortHelper::getDirection($request));
        }

        if (!is_null($request->filters)) {
            foreach ($request->filters as $column => $value) {
                if (!in_array($column, $this->enabledFilters)) {
                    continue;
                }
                if (is_null($value)) {
                    continue;
                }

                $column = empty($this->alias) ? $column : ($this->alias . '.' . $column);

                if ($strictFilters) {
                    $this->query->where($column, '=', $value);
                } else {
                    $this->query->where($column, 'like', '%' . $value . '%');
                }
            }
        }
    }

    public function getCount(): int
    {
        if ($this->calculateTotalCount) {
            return parent::getCount();
        }
        return 1000;
    }

}
