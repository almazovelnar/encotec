<?php

namespace App\Helpers;

use Illuminate\Support\Str;

final class StringHelper
{
    public static function clearTokens(string $content): string
    {
        $content = preg_replace('/({{photo-token-[0-9]+}})/i', '', $content);
        return preg_replace('/({{photo-token-[0-9]+}})/i', '', $content);
    }

    public static function clearSearchQuery(?string $string): string
    {
        $string = preg_replace('/[^\p{L}\:\d]/u', ' ', $string);
        return preg_replace('/(\s{1})\1*/ui', ' ', trim($string));
    }

    public static function parseYoutubeCode(string $string): ?string
    {
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $string, $matches);
        return $matches[0] ?? $string;
    }

    public static function generateSlug(string $string, ?string $language, ?string $separator = '-')
    {
        $slug = Str::slug($string, $separator, $language ?? config('app.locale'));
        return $slug;
    }

    public static function uniqueId()
    {
        return md5(uniqid().time());
    }

    public static function searchableString(?string $string): ?string
    {
        if ($string) {
            $string = str_replace(
                ['ç', 'ch', 'ə', 'ö', 'ş', 'sh', 'ü', 'ğ', 'ı', 'İ', 'Ç', 'Ch', 'Ş', 'Sh', 'Ğ', 'Ə', 'Ü', 'Ö', 'I', 'kh', 'q', 'Kh', 'Q'],
                ['c', 'c', 'e', 'o', 's', 's', 'u', 'g', 'i', 'i', 'c', 'c', 's', 's', 'g', 'e', 'u', 'o', 'i', 'g', 'g', 'g', 'g'],
                $string
            );
        }

        return $string;
    }

    public static function removeEmojis($string): string
    {
        // Match Enclosed Alphanumeric Supplement
        $regex_alphanumeric = '/[\x{1F100}-\x{1F1FF}]/u';
        $clear_string = preg_replace($regex_alphanumeric, '', $string);

        // Match Miscellaneous Symbols and Pictographs
        $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clear_string = preg_replace($regex_symbols, '', $clear_string);

        // Match Emoticons
        $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clear_string = preg_replace($regex_emoticons, '', $clear_string);

        // Match Transport And Map Symbols
        $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clear_string = preg_replace($regex_transport, '', $clear_string);

        // Match Supplemental Symbols and Pictographs
        $regex_supplemental = '/[\x{1F900}-\x{1F9FF}]/u';
        $clear_string = preg_replace($regex_supplemental, '', $clear_string);

        // Match Miscellaneous Symbols
        $regex_misc = '/[\x{2600}-\x{26FF}]/u';
        $clear_string = preg_replace($regex_misc, '', $clear_string);

        // Match Dingbats
        $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
        $clear_string = preg_replace($regex_dingbats, '', $clear_string);

        return $clear_string;
    }
}
