<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use RuntimeException;
use App\Models\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\BranchService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Branch\{BranchCreateRequest, BranchUpdateRequest};

class BranchController extends Controller
{
    private $branchService;

    public function __construct(BranchService $branchService)
    {
        $this->branchService = $branchService;
    }

    public function index()
    {
        $query = Branch::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.branches.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.branches.create');
    }

    public function store(BranchCreateRequest $request)
    {
        try {
            $this->branchService->create($request);
            return redirect()->route('admin.branches.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.branches.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function show(Branch $branch)
    {
        return view('backend.branches.view', [
            'branch' => $branch
        ]);
    }

    public function edit(Branch $branch)
    {
        return view('backend.branches.update', compact('branch'));
    }

    public function update(BranchUpdateRequest $request, Branch $branch)
    {
        try {
            $this->branchService->update($request, $branch);
            return redirect()->route('admin.branches.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.branches.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Branch $branch): RedirectResponse
    {
        try {
            $this->branchService->remove($branch);
            return redirect()->route('admin.branches.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.branches.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $branch = Branch::findOrFail($id);
        ($type == 'up') ? $branch->up() : $branch->down();

        return redirect()->back();
    }
}
