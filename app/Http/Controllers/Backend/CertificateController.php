<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use RuntimeException;
use App\Models\Certificate;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\CertificateService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Certificate\{CertificateCreateRequest, CertificateUpdateRequest};

class CertificateController extends Controller
{
    public function __construct(private CertificateService $certificateService)
    {
    }

    public function index()
    {
        $query = Certificate::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.certificates.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.certificates.create');
    }

    public function store(CertificateCreateRequest $request)
    {
        try {
            $this->certificateService->create($request);
            return redirect()->route('admin.certificates.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.certificates.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Certificate $certificate)
    {
        return view('backend.certificates.update', compact('certificate'));
    }

    public function update(CertificateUpdateRequest $request, Certificate $certificate)
    {
        try {
            $this->certificateService->update($request, $certificate);
            return redirect()->route('admin.certificates.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.certificates.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Certificate $certificate): RedirectResponse
    {
        try {
            $this->certificateService->remove($certificate);
            return redirect()->route('admin.certificates.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.certificates.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $page = Certificate::findOrFail($id);
        ($type == 'up') ? $page->up() : $page->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->certificateService->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }

    public function fileDelete(int $id, string $lang)
    {
        try {
            $this->certificateService->removeFile($id, $lang);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
