<?php

namespace App\Http\Controllers\Backend;

use App\Models\Config;
use App\Http\Controllers\Controller;
use App\Services\manager\ConfigService;
use App\Http\Requests\Config\ConfigRequest;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class ConfigController extends Controller
{
    private $configService;

    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    public function index()
    {
        $dataProvider = new EloquentDataProvider(Config::query()->orderByDesc('id'));
        return view('backend.configs.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.configs.create');
    }

    public function store(ConfigRequest $request)
    {
        try {
            $this->configService->create($request);
            return redirect()->route('admin.config.index')
                ->with('success', trans('form.success.save'));
        }
        catch (\Exception $e) {
            return redirect()->route('admin.config.index')
                ->with('error', trans('form.success.error'));
        }
    }

    public function edit(Config $config)
    {
        return view('backend.configs.update', [
            'config' => $config
        ]);
    }

    public function update(ConfigRequest $request, Config $config)
    {
        try {
            $this->configService->update($config, $request);
            return redirect()->route('admin.config.index')
                ->with('success', trans('form.success.save'));
        } catch (\Exception $e) {
            return redirect()->route('admin.config.index')
                ->with('error', trans('form.success.error'));
        }
    }

    public function destroy(Config $config)
    {
        try {
            $this->configService->remove($config);
            return redirect()->route('admin.config.index');
        } catch (\Exception $e) {
            return redirect()->route('admin.config.index');
        }
    }
}
