<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use RuntimeException;
use App\Models\Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\ImageService;
use App\Http\Requests\Image\{ImageRequest};
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class ImageController extends Controller
{
    public function __construct(private ImageService $imageService)
    {
    }

    public function index()
    {
        $query = Image::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.images.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.images.create');
    }

    public function store(ImageRequest $request)
    {
        try {
            $this->imageService->create($request);
            return redirect()->route('admin.images.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            dd($e);
            return redirect()->route('admin.images.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Image $image)
    {
        return view('backend.images.update', compact('image'));
    }

    public function update(ImageRequest $request, Image $image)
    {
        try {
            $this->imageService->update($request, $image);
            return redirect()->route('admin.images.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.images.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Image $image): RedirectResponse
    {
        try {
            $this->imageService->remove($image);
            return redirect()->route('admin.images.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.images.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $page = Image::findOrFail($id);
        ($type == 'up') ? $page->up() : $page->down();

        return redirect()->back();
    }
}
