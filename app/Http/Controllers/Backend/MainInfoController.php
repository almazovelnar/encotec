<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\Http\Controllers\Controller;
use App\Repositories\MainInfoRepository;
use App\Services\manager\MainInfoService;
use App\Http\Requests\MainInfo\MainInfoRequest;

class MainInfoController extends Controller
{
    private $mainInfoService;
    private $mainInfoRepository;

    public function __construct(
        MainInfoService $mainInfoService,
        MainInfoRepository $mainInfoRepository
    ) {
        $this->mainInfoService = $mainInfoService;
        $this->mainInfoRepository = $mainInfoRepository;
    }

    public function index()
    {
        return view('backend.main-info.index', [
            'info' => $this->mainInfoRepository->getFirst()
        ]);
    }

    public function update(MainInfoRequest $request)
    {
        $model = $this->mainInfoRepository->getFirst();

        try {
            $this->mainInfoService->update($request, $model);
            $this->mainInfoService->flushCache();

            return redirect()->route('admin.main-info')
                ->with('success', trans('form.success.save'));
        }
        catch(Exception $e) {}
    }

    public function deleteLogo(int $id, string $lang)
    {
        try {
            $this->mainInfoService->deleteLogo($id, $lang);
            return response()->json(trans('admin.success_deleted'));
        } catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function deleteFavicon(int $id, string $lang)
    {
        try {
            $this->mainInfoService->deleteFavicon($id, $lang);
            return response()->json(trans('admin.success_deleted'));
        } catch(Exception $e) {
            return response()->json($e->getMessage());
        }
    }
}
