<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use RuntimeException;
use App\Models\Partner;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\PartnerService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Partner\{PartnerCreateRequest, PartnerUpdateRequest};

class PartnerController extends Controller
{
    public function __construct(private PartnerService $partnerService)
    {
    }

    public function index()
    {
        $query = Partner::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.partners.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.partners.create');
    }

    public function store(PartnerCreateRequest $request)
    {
        try {
            $this->partnerService->create($request);
            return redirect()->route('admin.partners.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.partners.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Partner $partner)
    {
        return view('backend.partners.update', compact('partner'));
    }

    public function update(PartnerUpdateRequest $request, Partner $partner)
    {
        try {
            $this->partnerService->update($request, $partner);
            return redirect()->route('admin.partners.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.partners.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Partner $partner): RedirectResponse
    {
        try {
            $this->partnerService->remove($partner);
            return redirect()->route('admin.partners.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.partners.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $page = Partner::findOrFail($id);
        ($type == 'up') ? $page->up() : $page->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->partnerService->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
