<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend\Post;

use Exception;
use RuntimeException;
use App\Models\Project\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\Project\CategoryService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Project\{CategoryCreateRequest, CategoryUpdateRequest};

class CategoryController extends Controller
{
    public function __construct(private CategoryService $categoryService)
    {
    }

    public function index()
    {
        $query = Category::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.project-categories.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.project-categories.create');
    }

    public function store(CategoryCreateRequest $request)
    {
        try {
            $this->categoryService->create($request);
            return redirect()->route('admin.project-categories.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.project-categories.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Category $project_category)
    {
        return view('backend.project-categories.update', [
            'category' => $project_category
        ]);
    }

    public function update(CategoryUpdateRequest $request, Category $project_category)
    {
        try {
            $this->categoryService->update($request, $project_category);
            return redirect()->route('admin.project-categories.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.project-categories.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Category $project_category): RedirectResponse
    {
        try {
            $this->categoryService->remove($project_category);
            return redirect()->route('admin.project-categories.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.project-categories.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $category = Category::findOrFail($id);
        ($type == 'up') ? $category->up() : $category->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->categoryService->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
