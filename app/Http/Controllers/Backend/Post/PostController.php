<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend\Post;

use Exception;
use RuntimeException;
use App\Models\Photo;
use App\Models\Post\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\Post\PostService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Post\{PostCreateRequest, PostUpdateRequest};

class PostController extends Controller
{
    public function __construct(private PostService $postService)
    {
    }

    public function index()
    {
        $query = Post::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.posts.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }
        return view('backend.posts.create', [
            'photos' => $photos
        ]);
    }

    public function store(PostCreateRequest $request)
    {
        try {
            $this->postService->create($request);
            return redirect()->route('admin.posts.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.posts.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Post $post)
    {
        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }
        if (empty($photos)) {
            $photos = $post->photos;
        }

        return view('backend.posts.update',
            compact('post', 'photos')
        );
    }

    public function update(PostUpdateRequest $request, Post $post)
    {
        try {
            $this->postService->update($request, $post);
            return redirect()->route('admin.posts.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.posts.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Post $post): RedirectResponse
    {
        try {
            $this->postService->remove($post);
            return redirect()->route('admin.posts.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.posts.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $post = Post::findOrFail($id);
        ($type == 'up') ? $post->up() : $post->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->postService->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }

    public function backgroundDelete(int $id)
    {
        try {
            $this->postService->removeBackground($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
