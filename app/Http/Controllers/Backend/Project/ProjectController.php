<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend\Project;

use Exception;
use RuntimeException;
use App\Models\Photo;
use App\Models\Project\Project;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Repositories\ProjectCategoryRepository;
use App\Services\manager\Project\ProjectService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Project\{ProjectCreateRequest, ProjectUpdateRequest};

class ProjectController extends Controller
{
    public function __construct(
        private ProjectCategoryRepository $projectCategoryRepository,
        private ProjectService $projectService
    )
    {
    }

    public function index()
    {
        $query = Project::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.projects.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }

        return view('backend.projects.create', [
            'categories' => $this->projectCategoryRepository->all(),
            'photos' => $photos
        ]);
    }

    public function store(ProjectCreateRequest $request)
    {
        try {
            $this->projectService->create($request);
            return redirect()->route('admin.projects.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.projects.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Project $project)
    {
        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }
        if (empty($photos)) {
            $photos = $project->photos;
        }

        return view('backend.projects.update', [
            'project' => $project,
            'photos' => $photos,
            'categories' => $this->projectCategoryRepository->all()
        ]);
    }

    public function update(ProjectUpdateRequest $request, Project $project)
    {
        try {
            $this->projectService->update($request, $project);
            return redirect()->route('admin.projects.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.projects.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Project $project): RedirectResponse
    {
        try {
            $this->projectService->remove($project);
            return redirect()->route('admin.projects.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.projects.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $service = Project::findOrFail($id);
        ($type == 'up') ? $service->up() : $service->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->projectService->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
