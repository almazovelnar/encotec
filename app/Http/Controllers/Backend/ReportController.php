<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use RuntimeException;
use App\Models\Report;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\ReportService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Report\{ReportCreateRequest, ReportUpdateRequest};

class ReportController extends Controller
{
    public function __construct(private ReportService $reportService)
    {
    }

    public function index()
    {
        $query = Report::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.reports.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.reports.create');
    }

    public function store(ReportCreateRequest $request)
    {
        try {
            $this->reportService->create($request);
            return redirect()->route('admin.reports.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.reports.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Report $report)
    {
        return view('backend.reports.update', compact('report'));
    }

    public function update(ReportUpdateRequest $request, Report $report)
    {
        try {
            $this->reportService->update($request, $report);
            return redirect()->route('admin.reports.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.reports.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Report $report): RedirectResponse
    {
        try {
            $this->reportService->remove($report);
            return redirect()->route('admin.reports.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.reports.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $page = Report::findOrFail($id);
        ($type == 'up') ? $page->up() : $page->down();

        return redirect()->back();
    }

    public function fileDelete(int $id, string $lang)
    {
        try {
            $this->reportService->removeFile($id, $lang);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
