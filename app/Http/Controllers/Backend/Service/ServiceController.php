<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend\Service;

use Exception;
use RuntimeException;
use App\Models\Photo;
use App\Models\Service\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\Service\ServiceService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Service\{ServiceCreateRequest, ServiceUpdateRequest};

class ServiceController extends Controller
{
    public function __construct(private ServiceService $service)
    {
    }

    public function index()
    {
        $query = Service::query()->orderByDesc('id');
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.services.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }
        return view('backend.services.create', [
            'photos' => $photos
        ]);
    }

    public function store(ServiceCreateRequest $request)
    {
        try {
            $this->service->create($request);
            return redirect()->route('admin.services.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.services.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Service $service)
    {
        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }
        if (empty($photos)) {
            $photos = $service->photos;
        }

        return view('backend.services.update',
            compact('service', 'photos')
        );
    }

    public function update(ServiceUpdateRequest $request, Service $service)
    {
        try {
            $this->service->update($request, $service);
            return redirect()->route('admin.services.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.services.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Service $service): RedirectResponse
    {
        try {
            $this->service->remove($service);
            return redirect()->route('admin.services.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.services.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $service = Service::findOrFail($id);
        ($type == 'up') ? $service->up() : $service->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->service->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }

    public function iconDelete(int $id)
    {
        try {
            $this->service->removeIcon($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
