<?php

namespace App\Http\Controllers\Backend;

use App\Models\Post;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;

class SiteController extends BaseController
{
    public function __construct(
//        private PostRepository     $postRepository,
//        private UserRepository     $userRepository
    )
    {
    }

    public function index()
    {
//        $activePostCount     = $this->postRepository->getActivePostsCount(\App::getLocale());
//        $activeAuthorsCount  = $this->userRepository->getAuthorsCount();
//        $activeAdminCount    = $this->userRepository->getAuthorsCount();
        return view('backend.site.index',
//            compact('activePostCount', 'activeAuthorsCount', 'activeAdminCount')
        );
    }
}
