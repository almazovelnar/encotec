<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use Exception;
use App\Models\Slide;
use RuntimeException;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\SlideService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Slide\{SlideCreateRequest, SlideUpdateRequest};

class SlideController extends Controller
{
    public function __construct(private SlideService $slideService)
    {
    }

    public function index()
    {
        $query = Slide::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.slides.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.slides.create');
    }

    public function store(SlideCreateRequest $request)
    {
        try {
            $this->slideService->create($request);
            return redirect()->route('admin.slides.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.slides.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Slide $slide)
    {
        return view('backend.slides.update', compact('slide'));
    }

    public function update(SlideUpdateRequest $request, Slide $slide)
    {
        try {
            $this->slideService->update($request, $slide);
            return redirect()->route('admin.slides.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.slides.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Slide $slide): RedirectResponse
    {
        try {
            $this->slideService->remove($slide);
            return redirect()->route('admin.slides.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.slides.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $page = Slide::findOrFail($id);
        ($type == 'up') ? $page->up() : $page->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->slideService->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
