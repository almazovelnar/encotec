<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use App\Repositories\PageRepository;

class PostController extends Controller
{
    private $page;

    public function __construct(
        private PostRepository $postRepository,
        private PageRepository $pageRepository
    )
    {
        $this->page = $this->pageRepository->getBySlug('posts');
    }

    public function index()
    {
        $this->generateMetaTag($this->page->title, \Str::words(strip_tags($this->page->description), 150), $this->page->meta);
        return view('frontend.posts', [
            'page' => $this->page,
            'posts' => $this->postRepository->all(6)
        ]);
    }

    public function view(string $slug)
    {
        $post = $this->postRepository->getBySlug($slug);
        $this->generateMetaTag($post->title, \Str::words(strip_tags($post->description), 150), $post->meta);
        return view('frontend.post', [
            'page' => $this->page,
            'post' => $post
        ]);
    }
}
