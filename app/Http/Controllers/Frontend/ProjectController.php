<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Repositories\PageRepository;
use App\Repositories\ProjectCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProjectRepository;

class ProjectController extends Controller
{
    public function __construct(
        private ProjectRepository $projectRepository,
        private PageRepository $pageRepository,
        private ProjectCategoryRepository $projectCategoryRepository
    )
    {
    }

    public function index(Request $request, ?string $categorySlug = null)
    {
        $page = $this->pageRepository->getBySlug('projects');
        $categories = $this->projectCategoryRepository->all();
        if ($categorySlug) {
            $category = $this->projectCategoryRepository->getBySlug($categorySlug);
            $projects = $this->projectRepository->getAllByCategory($category->id);

            $this->generateMetaTag($category->title, \Str::words(strip_tags($category->description), 150));
        } else {
            $projects = $this->projectRepository->all(20);

            $this->generateMetaTag($page->title, \Str::words(strip_tags($page->description), 150), $page->meta);
        }

        return view('frontend.projects', [
            'page' => $page,
            'category' => $category ?? null,
            'categories' => $categories,
            'projects' => $projects
        ]);
    }

    public function view(Request $request, string $slug)
    {
        $project = $this->projectRepository->getBySlug($slug);

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'html' => view('frontend.partials._project', [
                    'project' => $project
                ])->render()
            ]);
        }
        return view('frontend.project', [
            'project' => $project
        ]);
    }
}
