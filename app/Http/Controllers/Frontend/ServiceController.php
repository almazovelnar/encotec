<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Repositories\PageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ServiceRepository;

class ServiceController extends Controller
{
    private $page;

    public function __construct(
        private ServiceRepository $serviceRepository,
        private PageRepository    $pageRepository
    )
    {
        $this->page = $this->pageRepository->getBySlug('services');
    }

    public function index()
    {
        $this->generateMetaTag($this->page->title, \Str::words(strip_tags($this->page->description), 150), $this->page->meta);
        return view('frontend.services', [
            'services' => $this->serviceRepository->all(20),
            'page'     => $this->page
        ]);
    }

    public function view(string $slug)
    {
        $service = $this->serviceRepository->getBySlug($slug);
        $nextService = $service->getNextSibling() ?? ($this->serviceRepository->firstWithExclude($service->id) ?? null);
        $otherServices = $this->serviceRepository->getWithExclude([$service->id, $nextService->id]);

        $this->generateMetaTag($service->title, $service->description);

        return view('frontend.service', [
            'page'          => $this->page,
            'service'       => $service,
            'nextService'   => $nextService,
            'otherServices' => $otherServices,
        ]);
    }
}
