<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Repositories\PartnerRepository;
use App\Repositories\PostRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SlideRepository;

class SiteController extends Controller
{
    public function __construct(
        private SlideRepository   $slideRepository,
        private ServiceRepository $serviceRepository,
        private PartnerRepository $partnerRepository,
        private ProjectRepository $projectRepository,
        private PostRepository    $postRepository
    )
    {
    }

    public function index(Request $request)
    {
        $this->generateMetaTag(site_info('title'), null, site_info('meta'), false);
        return view('frontend.home', [
            'slides'   => $this->slideRepository->all(10),
            'services' => $this->serviceRepository->all(6),
            'clients'  => $this->partnerRepository->all(),
            'projects' => $this->projectRepository->all(5),
            'posts'    => $this->postRepository->all(4)
        ]);
    }
}
