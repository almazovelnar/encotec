<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Page\ContactUsRequest;
use App\Repositories\Cacheable\CacheablePageRepository;
use App\Repositories\CertificateRepository;
use App\Repositories\PartnerRepository;
use App\Repositories\ReportRepository;
use Illuminate\Http\Request;

class StaticController extends Controller
{

    public function __construct(
        private CacheablePageRepository $cacheablePageRepository,
        private PartnerRepository $partnerRepository,
        private CertificateRepository $certificateRepository,
        private ReportRepository $reportRepository
    )
    {
    }

    public function view(string $slug)
    {
        $page = $this->cacheablePageRepository->getBySlug($slug);
        $this->generateMetaTag($page->title, \Str::words(strip_tags($page->description), 150), $page->meta);

        return view('frontend.static', [
            'page' => $page
        ]);
    }

    public function about(Request $request)
    {
        $page = $this->cacheablePageRepository->getBySlug('about-us');

        $this->generateMetaTag($page->title, \Str::words(strip_tags($page->description), 150), $page->meta);
        return view('frontend.about', [
            'page' => $page,
            'clients' => $this->partnerRepository->all(15),
            'reports' => $this->reportRepository->all(15),
            'certificates' => $this->certificateRepository->all(15),
        ]);
    }
}
