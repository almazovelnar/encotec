<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Helpers\StringHelper;

class SearchQuery
{
    public function handle(Request $request, Closure $next)
    {
        $request->validate([
            'query' => ['required', 'string', 'min:3']
        ]);

        $filteredQuery = StringHelper::clearSearchQuery($query = $request->get('query'));

        if (strcmp($query, $filteredQuery) <> 0 || $request->isMethod('post')) {
            return redirect()->route('search', ['query' => $filteredQuery]);
        }

        return $next($request);
    }
}
