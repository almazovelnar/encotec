<?php

namespace App\Http\Requests\Config;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'param' => ['required'],
//            'value' => ['required'],
//            'default' => ['required'],
            'label' => ['required'],
            'type' => ['required']
        ];
    }
}
