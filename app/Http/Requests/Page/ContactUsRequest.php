<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ContactUsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                 => 'required|string|min:3',
            'email'                => 'required|email',
            'message'              => 'required|string|min:10',
            'g-recaptcha-response' => 'required|recaptchav3:captcha,0.5',
        ];
    }

    public function messages()
    {
        return [
            '*.required' => trans('site.validation.required'),
            '*.min'      => trans('site.validation.min', ['min' => ':min']),
            '*.email'    => trans('site.validation.email'),
        ];
    }

}
