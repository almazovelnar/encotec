<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;

class PageUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => ['array'],
            'title.*'       => ['required', 'min:3'],
            'description'   => ['array'],
            'description.*' => ['nullable', 'min:10'],
            'image'         => ['nullable', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
