<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class ProjectCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => ['required'],
            'title'         => ['array'],
            'title.*'       => ['required', 'min:3'],
            'description'   => ['array'],
            'description.*' => ['required', 'min:10'],
            'photos'        => ['array', 'min:1'],
            'image'         => ['required', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')],
        ];
    }

    public function messages()
    {
        return [
            'photos.array' => trans('validation.required'),
            'photos.min' => trans('validation.required')
        ];
    }
}
