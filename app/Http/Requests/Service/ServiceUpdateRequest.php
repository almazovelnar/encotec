<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class ServiceUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => ['array'],
            'title.*'       => ['required', 'min:3'],
            'description'   => ['array'],
            'description.*' => ['required', 'min:10'],
            'image'         => ['nullable', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')],
            'icon'          => ['nullable', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')],
        ];
    }
}
