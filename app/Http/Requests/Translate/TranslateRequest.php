<?php

namespace App\Http\Requests\Translate;

use Illuminate\Foundation\Http\FormRequest;

class TranslateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'group' => ['required'],
            'key' => ['required', 'min:2'],
            'text' => 'array',
            'text.*' => ['required']
        ];
    }
}
