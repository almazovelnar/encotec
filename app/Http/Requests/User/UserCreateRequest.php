<?php

namespace App\Http\Requests\User;

use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class UserCreateRequest extends UserRequest
{
    public function rules()
    {
        return [
            'name'                  => ['required', 'max:50', 'min:3'],
            'email'                 => ['required', 'email', 'min:6', 'unique:users'],
            'password'              => ['required', 'min:6'],
            'password_confirmation' => ['required', 'same:password'],
            'role'                  => Rule::in(Role::get()->pluck('id')),
            'thumb'                 => ['nullable', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
