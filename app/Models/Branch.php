<?php

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $address
 * @property string $image
 * @property array $phone
 * @property array $email
 * @property boolean $status
 */
class Branch extends Model
{
    use HasTranslations, NodeTrait;

    public $translatable = ['title', 'description', 'address'];

    protected $casts = [
        'phone' => 'object',
        'email' => 'object',
    ];

    public static function create(array $title, array $description, array $address, array $phone, array $email, bool $status)
    {
        $model = new static();

        $model->title = $title;
        $model->description = $description;
        $model->address = $address;
        $model->phone = $phone;
        $model->email = $email;
        $model->status = $status;

        return $model;
    }

    public function edit(array $title, array $description, array $address, array $phone, array $email, bool $status)
    {
        $this->title = $title;
        $this->description = $description;
        $this->address = $address;
        $this->phone = $phone;
        $this->email = $email;
        $this->status = $status;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
