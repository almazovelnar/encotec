<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $file
 * @property bool $status
 */
class Certificate extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['title', 'image', 'file'];

    public static function create(array $title, bool $status)
    {
        $model = new static();

        $model->title = $title;
        $model->status = $status;

        return $model;
    }

    public function edit(array $title, bool $status)
    {
        $this->title = $title;
        $this->status = $status;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
