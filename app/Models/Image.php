<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $param
 * @property string $label
 * @property int $photo_id
 */
class Image extends Model
{
    public $timestamps = false;

    public static function create(string $param, int $photoId, ?string $label): self
    {
        $model = new static();

        $model->param = $param;
        $model->photo_id = $photoId;
        $model->label = $label;

        return $model;
    }

    public function edit(string $param, int $photoId, ?string $label)
    {
        $this->param = $param;
        $this->photo_id = $photoId;
        $this->label = $label;
    }

    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
}
