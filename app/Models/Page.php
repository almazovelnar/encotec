<?php

namespace App\Models;

use Str;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use App\Queries\CachedQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $template
 * @property bool $status
 * @property bool $on_menu
 * @property bool $with_contacts
 * @property array $meta
 */
class Page extends Model
{
    use HasTranslations, NodeTrait, Cachable;

    public $translatable = ['title', 'description', 'meta'];

    protected $casts = [
        'meta' => 'object',
    ];

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;

            return new QueryBuilder($query);
        }

        return new CachedQueryBuilder($query);
    }

    public static function create(array $title, array $description, array $meta, bool $status, bool $menu, string $template, string $slug)
    {
        $model = new static();

        $model->title = $title;
        $model->description = $description;
        $model->meta = $meta;
        $model->status = $status;
        $model->on_menu = $menu;
        $model->template = $template;
        $model->slug = $slug;

        return $model;
    }

    public function edit(array $title, array $description, array $meta, bool $status, bool $menu, string $slug)
    {
        $this->title = $title;
        $this->description = $description;
        $this->meta = $meta;
        $this->status = $status;
        $this->on_menu = $menu;
        $this->slug = $slug;
    }

    public function replicate(array $except = null)
    {
        return parent::replicate($except);
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
