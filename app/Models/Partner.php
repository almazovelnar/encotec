<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $link
 * @property string $image
 * @property bool $status
 */
class Partner extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['title', 'link'];

    public static function create(array $title, array $link, bool $status)
    {
        $model = new static();

        $model->title = $title;
        $model->link = $link;
        $model->status = $status;

        return $model;
    }

    public function edit(array $title, array $link, bool $status)
    {
        $this->title = $title;
        $this->link = $link;
        $this->status = $status;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
