<?php

namespace App\Models\Post;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $info
 * @property string $slug
 * @property string $image
 * @property string $background
 * @property bool $status
 */
class Post extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['title', 'description', 'info', 'slug'];

    public static function create(array $title, array $description, array $info, array $slug, bool $status)
    {
        $model = new static();

        $model->title = $title;
        $model->description = $description;
        $model->info = $info;
        $model->slug = $slug;
        $model->status = $status;

        return $model;
    }

    public function edit(array $title, array $description, array $info, array $slug, bool $status)
    {
        $this->title = $title;
        $this->description = $description;
        $this->info = $info;
        $this->status = $status;
        $this->slug = $slug;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }

    public function attachBackground($background)
    {
        $this->background = $background;
    }

    public function detachBackground()
    {
        $this->background = null;
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'post_photo')->withTimestamps();
    }
}
