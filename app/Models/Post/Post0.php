<?php
declare(strict_types=1);

namespace App\Models\Post;

use App\Enum\Status;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;


/**
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $description
 * @property string $language
 * @property string $youtube_id
 * @property string $video
 * @property string $duration
 * @property string $size
 * @property string $story_video
 * @property string $image
 * @property string $slug
 * @property string $channel_id
 * @property bool $is_active
 * @property bool $status
 * @property bool $editor_choice
 * @property bool $with_comments
 * @property bool $is_slider
 * @property bool $is_top
 * @property bool $is_story
 * @property int $views
 * @property int $likes
 * @property int $dislikes
 * @property int $user_id
 * @property int|null $parent_id
 * @property int|null $city_id
 * @property int|null $author_id
 * @property int|null $group_id
 *
 * @property string $type
 * @property array $meta
 *
 * @property Category $category
 * @property Collection<Photo> $photos
 */
class Post0 extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = [];
    protected $casts = [
        'meta'         => 'object',
        'marked_words' => 'object',
    ];

    protected $fillable = ['group_id', 'is_active'];


    public static function create($lang, $category, $user, $author, bool $status, bool $editorChoice, bool $slider, bool $is_top, bool $is_story, bool $with_comments, $type, $channel, string $title, string $description, $meta, $marked_words, $youtube, $slug, $date, $city)
    {
        $model = new static();

        $model->language = $lang;
        $model->title = $title;
        $model->description = $description;
        $model->status = $status;
        $model->editor_choice = $editorChoice;
        $model->is_slider = $slider;
        $model->is_top = $is_top;
        $model->is_story = $is_story;
        $model->with_comments = $with_comments;
        $model->category_id = $category;
        $model->user_id = $user;
        $model->author_id = $author;
        $model->city_id = $city;
        $model->type = $type;
        $model->channel_id = $channel;
        $model->youtube_id = $youtube;
        $model->meta = $meta;
        $model->marked_words = $marked_words;
        $model->slug = $slug;
        $model->published_at = $date ?? now();

        if ($model->published_at < now() && $status) {
            $model->is_active = Status::ACTIVE;
        } else {
            $model->is_active = Status::INACTIVE;
        }
        return $model;
    }

    public function edit($category, $user, $author, bool $status, bool $editorChoice, bool $slider, bool $is_top, bool $is_story, bool $with_comments, string $type, $views, $likes, $dislikes, $channel, string $title, string $description, $meta, $marked_words, $youtube, $slug, $date, $city)
    {
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;
        $this->editor_choice = $editorChoice;
        $this->is_slider = $slider;
        $this->is_top = $is_top;
        $this->is_story = $is_story;
        $this->with_comments = $with_comments;
        $this->category_id = $category;
//        $this->user_id = $user;
        $this->author_id = $user;
        $this->city_id = $city;
        $this->type = $type;
        $this->views = $views;
        $this->likes = $likes;
        $this->dislikes = $dislikes;
        $this->channel_id = $channel;
        $this->youtube_id = $youtube;
        $this->meta = $meta;
        $this->marked_words = $marked_words;
        $this->slug = $slug;
        $this->published_at = $date ?? now();
        if ($this->published_at < now() && $status) {
            $this->is_active = Status::ACTIVE;
        } else {
            $this->is_active = Status::INACTIVE;
        }
    }

    public function getMarkedWords()
    {
        return $this->marked_words;
    }

    public function meta()
    {
        return $this->meta;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }

    public function attachVideo($video)
    {
        $this->video = $video;
    }

    public function detachVideo()
    {
        $this->video = null;
    }

    public function attachDuration($duration)
    {
        $this->duration = $duration;
    }

    public function detachDuration()
    {
        $this->duration = null;
    }

    public function attachSize($size)
    {
        $this->size = $size;
    }

    public function detachSize()
    {
        $this->size = null;
    }

    public function attachStoryVideo($video)
    {
        $this->story_video = $video;
    }

    public function detachStoryVideo()
    {
        $this->story_video = null;
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            'post_tag'
        );
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'post_photo')->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getContentAttribute()
    {
        return preg_replace(['/{{photo-token-([a-z0-9\-]+)}}/i', '/{{widget-token-([a-z0-9\-]+)}}/i'], '', $this->description);
    }
}
