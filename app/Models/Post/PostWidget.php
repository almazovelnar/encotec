<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;

class PostWidget extends Model
{
    protected $table = 'widgets';
}
