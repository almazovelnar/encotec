<?php

namespace App\Models\Project;

use Str;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use App\Queries\CachedQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Cviebrock\EloquentSluggable\Sluggable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $image
 * @property bool $status
 */
class Category extends Model
{
    use HasFactory, HasTranslations, NodeTrait;

    protected $table = 'project_categories';
    public $translatable = ['title', 'slug', 'description'];

    public static function create(array $title, array $description, array $slug, bool $status)
    {
        $model = new static();

        $model->title = $title;
        $model->description = $description;
        $model->slug = $slug;
        $model->status = $status;

        return $model;
    }

    public function edit(array $title, array $description, array $slug, bool $status)
    {
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;
        $this->slug = $slug;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
