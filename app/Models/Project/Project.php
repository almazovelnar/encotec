<?php

namespace App\Models\Project;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $image
 * @property string $icon
 * @property bool $status
 */
class Project extends Model
{
    use HasFactory, HasTranslations;

    public $translatable = ['title', 'slug', 'description'];

    public static function create($category_id, array $title, array $description, array $slug, bool $status)
    {
        $model = new static();

        $model->category_id = $category_id;
        $model->title = $title;
        $model->description = $description;
        $model->slug = $slug;
        $model->status = $status;

        return $model;
    }

    public function edit($category_id, array $title, array $description, array $slug, bool $status)
    {
        $this->category_id = $category_id;
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;
        $this->slug = $slug;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'project_photo')->withTimestamps();
    }
}
