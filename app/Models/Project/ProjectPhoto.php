<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectPhoto extends Model
{
    public $timestamps = true;
    protected $table = 'project_photo';

    public static function create($projectId, $photoId, $sortPhoto)
    {
        $model = new static();

        $model->project_id = $projectId;
        $model->photo_id = $photoId;
        $model->sort = $sortPhoto;

        return $model;
    }
}
