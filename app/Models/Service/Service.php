<?php

namespace App\Models\Service;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $image
 * @property string $icon
 * @property bool $status
 */
class Service extends Model
{
    use HasFactory, HasTranslations, NodeTrait;

    public $translatable = ['title', 'slug', 'description'];

    public static function create(array $title, array $description, array $slug, bool $status)
    {
        $model = new static();

        $model->title = $title;
        $model->description = $description;
        $model->slug = $slug;
        $model->status = $status;

        return $model;
    }

    public function edit(array $title, array $description, array $slug, bool $status)
    {
        $this->title = $title;
        $this->description = $description;
        $this->status = $status;
        $this->slug = $slug;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }

    public function attachIcon($icon)
    {
        $this->icon = $icon;
    }

    public function detachIcon()
    {
        $this->icon = null;
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'service_photo')
            ->orderBy('sort')
            ->limit(3)
            ->withTimestamps();
    }
}
