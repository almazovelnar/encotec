<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Model;

class ServicePhoto extends Model
{
    public $timestamps = true;

    protected $table = 'service_photo';

    public static function create($serviceId, $photoId, $sortPhoto)
    {
        $model = new static();

        $model->service_id = $serviceId;
        $model->photo_id = $photoId;
        $model->sort = $sortPhoto;

        return $model;
    }
}
