<?php

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $link
 * @property string $image
 * @property bool $status
 */
class Slide extends Model
{
    use HasFactory, HasTranslations, NodeTrait;

    public $translatable = ['title', 'description', 'link'];

    public static function create(array $title, array $description, array $link, bool $status)
    {
        $model = new static();

        $model->title = $title;
        $model->description = $description;
        $model->link = $link;
        $model->status = $status;

        return $model;
    }

    public function edit(array $title, array $description, array $link, bool $status)
    {
        $this->title = $title;
        $this->description = $description;
        $this->link = $link;
        $this->status = $status;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
