<?php

namespace App\Models;

use Spatie\TranslationLoader\LanguageLine;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Translate extends LanguageLine
{
    use HasFactory;

    protected $table = 'language_lines';

    public $casts = [
        'text' => 'array'
    ];

    public static function create($group, $key, $text)
    {
        $model = new static();
        $model->group = $group;
        $model->key = $key;
        $model->text = $text;
        return $model;
    }

    public function edit($group, $key, $text) {
        $this->group = $group;
        $this->key = $key;
        $this->text = $text;
    }
}
