<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Branch;
use Illuminate\Database\Eloquent\Builder;

class BranchRepository
{
    public function get($id)
    {
        return Branch::find($id);
    }

    public function thisQuery(): Builder
    {
        return Branch::where('status', Status::ACTIVE);
    }

    public function all(?int $limit = 10)
    {
        return $this->thisQuery()
            ->defaultOrder()
            ->limit($limit)
            ->get();
    }
}
