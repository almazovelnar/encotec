<?php

namespace App\Repositories\Cacheable;

use App\Repositories\CategoryRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class CacheableCategoryRepository
{
    private string $language;

    public function __construct(
        private CategoryRepository $categoryRepository,
        private CacheablePostRepository $cacheablePostRepository
    )
    {
        $this->language = App::getLocale();
    }

    public function getChosenCategoriesWithPosts(int $categoryLimit, int $postsLimit)
    {
        $categories = Cache::remember(
            'categories.columns.' . App::getLocale(), 3600, function () use ($categoryLimit, $postsLimit) {
            return $this->categoryRepository->getChosen($categoryLimit);
        });

        foreach ($categories as $category) {
            $category->posts = $this->cacheablePostRepository->getPostsByCategory($category->id, $postsLimit);
        }

        return $categories;
    }

    public function getBySlug(string $slug)
    {
        return Cache::remember('category.slug.' . $slug, 21600, function () use ($slug) {
            return $this->categoryRepository->getBySlug($slug);
        });
    }

    public function getMenuChosens(?int $limit = 24)
    {
        return Cache::remember('categories.chosen.' . $this->language, 21600, function () use ($limit) {
            return $this->categoryRepository->getMenuChosens($limit);
        });
    }

    public function getTranslateByLocale(int $groupId, string $locale)
    {
        return Cache::remember('category.translate.' . $groupId . '.' . $locale, 21600, function () use ($groupId, $locale) {
            return $this->categoryRepository->getTranslateByLocale($groupId, $locale);
        });
    }

    public function getMenuOthers(?int $limit = 24)
    {
        return Cache::remember('categories.other.' . $this->language, 21600, function () use ($limit) {
            return $this->categoryRepository->getMenuOthers($limit);
        });
    }

}
