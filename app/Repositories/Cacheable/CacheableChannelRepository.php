<?php

namespace App\Repositories\Cacheable;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use App\Repositories\ChannelRepository;

class CacheableChannelRepository
{
    private string $language;

    public function __construct(private ChannelRepository $channelRepository)
    {
        $this->language = App::getLocale();
    }

    public function getBySlug(string $slug)
    {
        return Cache::remember('channel.slug.' . $slug, 21600, function () use ($slug) {
            return $this->channelRepository->getBySlug($slug);
        });
    }

    public function getChosens(?int $limit = 24)
    {
        return Cache::remember('channels.chosen.' . $this->language, 21600, function () use ($limit) {
            return $this->channelRepository->getChosens($limit);
        });
    }

    public function getOthers(?int $limit = 24)
    {
        return Cache::remember('channels.other.' . $this->language, 21600, function () use ($limit) {
            return $this->channelRepository->getOthers($limit);
        });
    }

    public function getTranslateByLocale(int $groupId, string $locale)
    {
        return Cache::remember('channel.translate.' . $groupId . '.' . $locale, 21600, function () use ($groupId, $locale) {
            return $this->channelRepository->getTranslateByLocale($groupId, $locale);
        });
    }
}
