<?php

namespace App\Repositories\Cacheable;

use Cache;
use App\Repositories\PageRepository;

class CacheablePageRepository
{

    public function __construct(private PageRepository $pageRepository)
    {
    }

    public function getBySlug(string $slug)
    {
        return Cache::remember('page.slug.' . $slug, 86400, function () use ($slug) {
            return $this->pageRepository->getBySlug($slug);
        });
    }

}
