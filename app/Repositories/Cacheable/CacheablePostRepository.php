<?php

namespace App\Repositories\Cacheable;

use App;
use App\Repositories\PostRepository;
use Illuminate\Support\Facades\Cache;

class CacheablePostRepository
{
    private string $language;

    public function __construct(
        private PostRepository $postRepository,
    )
    {
        $this->language = App::getLocale();
    }

    public function getChosenPosts(?int $limit = 10, ?string $lastPub = null)
    {
        if ($lastPub) {
            return $this->postRepository->getChosenPosts($limit, $lastPub);
        }

        return Cache::remember('posts.chosen.' . $this->language, 3600, function () use ($limit) {
            return $this->postRepository->getChosenPosts($limit);
        });
    }

    public function getBySlug(string $slug)
    {
        return Cache::remember('post.slug.' . $slug, 3600, function () use ($slug) {
            return $this->postRepository->getBySlug($slug);
        });
    }

    public function getByOldId(int $oldId)
    {
        return Cache::remember('post.oldId.' . $oldId, 86400 * 30, function () use ($oldId) {
            return $this->postRepository->getByOldId($oldId);
        });
    }

    public function getById(int $id)
    {
        return Cache::remember('post.id.' . $id, 3600, function () use ($id) {
            return $this->postRepository->getById($id);
        });
    }

    public function getByTag(int $tagId, int $limit, ?string $lastDate = null)
    {
        if ($lastDate) {
            return $this->postRepository->getByTag($tagId, $limit, $lastDate);
        }

        return Cache::remember('posts.tag.' . $tagId, 3600, function () use ($tagId, $limit) {
            return $this->postRepository->getByTag($tagId, $limit);
        });
    }

    public function getStories()
    {
        return Cache::remember('posts.stories.' . $this->language, 86400, function () {
            return $this->postRepository->getStories();
        });
    }

    public function getPostTranslates(int $id, int $groupId, string $language)
    {
        return Cache::remember('posts.translates.' . $id, 3600, function () use ($groupId, $language) {
            return $this->postRepository->getPostTranslates($groupId, $language);
        });
    }

    public function getRelatedPosts(int $catId, int $postId, int $limit)
    {
        return Cache::remember('posts.related.' . $postId, 1800, function () use ($catId, $postId, $limit) {
            return $this->postRepository->getRelatedPosts($catId, $postId, $limit);
        });
    }

    public function getLastPostByCategory(int $catId)
    {
        return Cache::remember('post.last.category.' . $catId, 1800, function () use ($catId) {
            return $this->postRepository->getLastPostByCategory($catId);
        });
    }

    public function getLatestByCategory(int $catId, $limit = 24, ?string $lastPub = null)
    {
        if ($lastPub) {
            return $this->postRepository->getLatestByCategory($catId, $limit, $lastPub);
        }

        return Cache::remember('posts.latest.category.' . $catId, 1800, function () use ($catId, $limit) {
            return $this->postRepository->getLatestByCategory($catId, $limit);
        });
    }

    public function getPostsByCategory(int $catId, $limit = 12)
    {
        return Cache::remember('posts.category.' . $catId, 1800, function () use ($catId, $limit) {
            return $this->postRepository->getLatestByCategory($catId, $limit);
        });
    }

    public function getChosenPostsByCategoryWithExclude(int $catId, int $excludeId, ?int $limit = 24, ?string $lastPub = null)
    {
        if ($lastPub) {
            return $this->postRepository->getChosenPostsByCategoryWithExclude($catId, $excludeId, $limit, $lastPub);
        }

        return Cache::remember('posts.chosen.exclude.' . $excludeId, 1800, function () use ($catId, $excludeId, $limit) {
            return $this->postRepository->getChosenPostsByCategoryWithExclude($catId, $excludeId, $limit);
        });
    }

    public function getLatestByChannel(int $channelId, $limit = 24, ?string $lastPub = null)
    {
        if ($lastPub) {
            return $this->postRepository->getLatestByChannel($channelId, $limit, $lastPub);
        }

        return Cache::remember('posts.latest.channel.' . $channelId, 1800, function () use ($channelId, $limit) {
            return $this->postRepository->getLatestByChannel($channelId, $limit);
        });
    }

    public function getChosenByCategory(int $catId, ?int $limit = 24, ?string $lastPub = null)
    {
        if ($lastPub) {
            return $this->postRepository->getChosenByCategory($catId, $limit, $lastPub);
        }

        return Cache::remember('posts.chosen.category.' . $catId, 3600, function () use ($catId, $limit) {
            return $this->postRepository->getChosenByCategory($catId, $limit);
        });
    }

    public function getChosenByChannel(int $channelId, ?int $limit = 24, ?string $lastPub = null)
    {
        if ($lastPub) {
            return $this->postRepository->getChosenByChannel($channelId, $limit, $lastPub);
        }

        return Cache::remember('posts.chosen.channel.' . $channelId, 3600, function () use ($channelId, $limit) {
            return $this->postRepository->getChosenByChannel($channelId, $limit);
        });
    }
}
