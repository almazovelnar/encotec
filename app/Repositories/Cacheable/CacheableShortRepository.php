<?php

namespace App\Repositories\Cacheable;

use App\Repositories\ShortRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class CacheableShortRepository
{
    private string $language;

    public function __construct(
        private ShortRepository $shortRepository
    )
    {
        $this->language = App::getLocale();
    }

    public function getBySlug(string $slug)
    {
        return Cache::remember('short.slug.' . $slug, 86400, function () use ($slug) {
            return $this->shortRepository->getBySlug($slug);
        });
    }

    public function getWithExclude(int $excludeId, $limit = 10, $lastPub = null)
    {
        if ($lastPub) {
            return $this->shortRepository->getWithExclude($excludeId, $limit, $lastPub);
        }

        return Cache::remember('shorts.exclude.' . $excludeId, 86400, function () use ($excludeId, $limit) {
            return $this->shortRepository->getWithExclude($excludeId, $limit);
        });
    }

    public function getLatest()
    {
        return Cache::remember('short.latest.' . $this->language, 3600, function () {
            return $this->shortRepository->getLatest();
        });
    }

    public function getAll(?int $limit = 10)
    {
        return Cache::remember('shorts.latest.' . $this->language, 3600, function () use ($limit) {
            return $this->shortRepository->all($limit);
        });
    }
}
