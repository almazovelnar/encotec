<?php

namespace App\Repositories\Cacheable;

use App\Repositories\TagRepository;
use Illuminate\Support\Facades\Cache;

class CacheableTagRepository
{
    public function __construct(
        private TagRepository $tagRepository
    )
    {
    }

    public function getBySlug(string $slug, string $lang)
    {
        return Cache::remember('tag.slug.' . $slug, 86400, function () use ($slug, $lang) {
            return $this->tagRepository->getBySlug($slug, $lang);
        });
    }

    public function getPopulars(string $lang, int $limit = 20)
    {
        return Cache::remember('tags.popular.' . $lang, 86400, function () use ($lang, $limit) {
            return $this->tagRepository->getPopulars($lang, $limit);
        });
    }

    public function getByPost(int $postId)
    {
        return Cache::remember('tags.post.' . $postId, 86400, function () use ($postId) {
            return $this->tagRepository->getByPost($postId);
        });
    }

    public function getChosens(string $lang, int $limit = 20)
    {
        return Cache::remember('tags.chosen.' . $lang, 86400, function () use ($lang, $limit) {
            return $this->tagRepository->getChosens($lang, $limit);
        });
    }

}
