<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Certificate;
use Illuminate\Database\Eloquent\Builder;

class CertificateRepository
{
    public function get($id)
    {
        return Certificate::find($id);
    }

    public function thisQuery(): Builder
    {
        return Certificate::query()
            ->where('status', Status::ACTIVE);
    }

    public function all(?int $limit = 10)
    {
        return $this->thisQuery()
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }
}
