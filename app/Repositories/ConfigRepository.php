<?php

namespace App\Repositories;

use App\Models\Config;

class ConfigRepository
{
    public function get($id)
    {
        return Config::get($id);
    }

    public function getSocials()
    {
        return Config::query()->where('type', 'social')->get();
    }
}
