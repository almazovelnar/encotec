<?php

namespace App\Repositories;

use App\Models\MainInfo;

class MainInfoRepository
{
    public function get($id)
    {
        return MainInfo::find($id);
    }

    public function getFirst()
    {
        return MainInfo::first();
    }
}
