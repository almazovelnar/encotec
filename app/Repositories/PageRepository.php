<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Page;
use Illuminate\Database\Eloquent\Builder;

class PageRepository
{
    public function get($id)
    {
        return Page::find($id);
    }

    public function getMenuItems(?int $limit = 12)
    {
        return $this->pageQuery()->whereNull('parent_id')->defaultOrder()->take($limit)->get();
    }

    public function getShowedItems(?int $limit = 12)
    {
        return $this->pageQuery()->whereNull('parent_id')->where('on_menu', true)->defaultOrder()->take($limit)->get();
    }

    public function getBySlug(string $slug)
    {
        return $this->pageQuery()->where('slug', $slug)->firstOrFail();
    }

    public function pageQuery(): Builder
    {
        return Page::where('status', Status::ACTIVE);
    }

    public function totalCount()
    {
        return Page::count();
    }
}
