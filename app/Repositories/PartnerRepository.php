<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Partner;
use Illuminate\Database\Eloquent\Builder;

class PartnerRepository
{
    public function get($id)
    {
        return Partner::find($id);
    }

    public function thisQuery(): Builder
    {
        return Partner::query()
            ->where('status', Status::ACTIVE);
    }

    public function all(?int $limit = 10)
    {
        return $this->thisQuery()
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }
}
