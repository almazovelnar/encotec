<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Builder;

class PostRepository
{
    public function get($id)
    {
        return Post::find($id);
    }

    public function thisQuery(): Builder
    {
        return Post::query()
            ->where('status', Status::ACTIVE);
    }

    public function all(?int $limit = 10)
    {
        return $this->thisQuery()
            ->orderByDesc('published_at')
            ->paginate($limit);
    }

    public function getBySlug(string $slug)
    {
        return $this->thisQuery()
            ->where('slug->' . app()->getLocale(), $slug)
            ->firstOrFail();
    }
}
