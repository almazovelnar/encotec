<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Project\Category;
use Illuminate\Database\Eloquent\Builder;

class ProjectCategoryRepository
{
    public function get($id)
    {
        return Category::find($id);
    }

    public function thisQuery(): Builder
    {
        return Category::where('status', Status::ACTIVE);
    }

    public function all()
    {
        return $this->thisQuery()
            ->defaultOrder()
            ->get();
    }

    public function getBySlug(string $slug)
    {
        return $this->thisQuery()
            ->where('slug->' . app()->getLocale(), $slug)
            ->first();
    }
}
