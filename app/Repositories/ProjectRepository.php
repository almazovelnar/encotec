<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Project\Project;
use Illuminate\Database\Eloquent\Builder;

class ProjectRepository
{
    public function get($id)
    {
        return Project::find($id);
    }

    public function thisQuery(): Builder
    {
        return Project::query()
            ->where('status', Status::ACTIVE);
    }

    public function all(?int $limit = 10)
    {
        return $this->thisQuery()
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }

    public function getAllByCategory(int $catId, ?int $limit = 10)
    {
        return $this->thisQuery()
            ->where('category_id', $catId)
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }

    public function getBySlug(string $slug)
    {
        return $this->thisQuery()
            ->where(\DB::raw('slug->"$.' . app()->getLocale() . '"'), $slug)
            ->firstOrFail();
    }
}
