<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Report;
use Illuminate\Database\Eloquent\Builder;

class ReportRepository
{
    public function get($id)
    {
        return Report::find($id);
    }

    public function thisQuery(): Builder
    {
        return Report::query()
            ->where('status', Status::ACTIVE);
    }

    public function all(?int $limit = 10)
    {
        return $this->thisQuery()
            ->orderByDesc('id')
            ->limit($limit)
            ->get();
    }
}
