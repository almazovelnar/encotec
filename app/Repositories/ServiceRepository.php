<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Certificate;
use App\Models\Service\Service;
use Illuminate\Database\Eloquent\Builder;

class ServiceRepository
{
    public function get($id)
    {
        return Service::find($id);
    }

    public function thisQuery(): Builder
    {
        return Service::query()
            ->where('status', Status::ACTIVE);
    }

    public function getBySlug(string $slug)
    {
        return $this->thisQuery()
            ->where('slug->' . app()->getLocale(), $slug)
            ->firstOrFail();
    }

    public function firstWithExclude(int $id)
    {
        return $this->thisQuery()
            ->where('id', '!=', $id)
            ->first();
    }

    public function getWithExclude(array $id)
    {
        return $this->thisQuery()
            ->whereNotIn('id', $id)
            ->get();
    }

    public function all(?int $limit = 10)
    {
        return $this->thisQuery()
            ->defaultOrder()
            ->limit($limit)
            ->get();
    }
}
