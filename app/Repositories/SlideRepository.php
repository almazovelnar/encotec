<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Slide;
use Illuminate\Database\Eloquent\Builder;

class SlideRepository
{
    public function get($id)
    {
        return Slide::find($id);
    }

    public function thisQuery(): Builder
    {
        return Slide::query()
            ->where('status', Status::ACTIVE);
    }

    public function all(?int $limit = 3)
    {
        return $this
            ->thisQuery()
            ->defaultOrder()
            ->limit($limit)
            ->get();
    }
}
