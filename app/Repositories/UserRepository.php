<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\User;

class UserRepository
{
    public function get($id)
    {
        return User::find($id);
    }

    public function getAuthors(?int $limit=100)
    {
        return User::query()->whereHas(
            'roles', function ($q) {
            $q->where('name', 'author');
        })
        ->where('status', Status::ACTIVE)
        ->limit($limit)
        ->get();
    }

    public function getAuthorById($id)
    {
        return User::query()
            ->whereHas(
                'roles', function ($q) {
                $q->where('name', 'author');
            })
            ->where('id', $id)
            ->where('status', Status::ACTIVE)
            ->first();

    }
    public function getAuthorsCount()
    {
        return User::query()->whereHas(
            'roles', function ($q) {
            $q->where('name', 'author');
        })
            ->where('status', Status::ACTIVE)
            ->count();
    }
    public function getAdminCount()
    {
        return User::query()->whereHas(
            'roles', function ($q) {
            $q->where('name', 'admin');
        })
            ->where('status', Status::ACTIVE)
            ->count();
    }
}
