<?php

namespace App\SearchQuery;

use App\Models\Post;

class PostSearchQuery implements Query
{

    public function search($filters = [])
    {
        $language = session()->get('current-lang') ?? config('app.admin_locale');

        $query = Post::query()
            ->from('posts as p')
            ->select(['p.*', 'c.title as category_title'])
            ->join('users as u', 'u.id', '=', 'p.user_id')
            ->join('categories as c', function ($join) use ($language) {
                $join->on('p.category_id', '=', 'c.id')->where('c.language', $language);
            })
            ->where('u.group_id', \Auth::user()->group_id)
            ->where('p.language', $language)
            ->orderByDesc('p.published_at');

        if (!empty($filters['title'])) {
            $query->where('p.title', 'like', "%{$filters['title']}%");
        }
        if (!empty($filters['published_at'])) {
            $query->where('p.published_at', 'like', "%{$filters['published_at']}%");
        }

        return $query;
    }

}
