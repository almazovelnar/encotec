<?php

namespace App\SearchQuery;

use App\Helpers\StringHelper;
use App\Models\Translate;

class TranslationSearchQuery implements Query
{

    public function search($filters = [])
    {
        $query = Translate::query()
            ->select(['id', 'group', 'key', 'text->' . $filters['language'] . ' as translatedLabel'])
            ->orderByDesc('id');

        if (!empty($filters['text'])) {
            $filters['text'] = mb_strtolower(StringHelper::clearSearchQuery($filters['text']));
            $query->where(\DB::raw('lower(text->"$.' . $filters['language'] . '")'), 'LIKE', "%{$filters['text']}%");
        }

        return $query;
    }

}
