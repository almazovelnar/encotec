<?php

namespace App\Services;

use Cache;

class CacheableDataService
{
    public function deletePageBySlugCache(string $slug)
    {
        $this->forgetCache('page.slug', $slug);
    }

    public function forgetCache(...$args)
    {
        Cache::forget(implode('.', $args));
    }
}
