<?php

declare(strict_types=1);

namespace App\Services\Sitemap;

use App\Services\Sitemap\Sources\SitemapSource;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use LaravelLocalization;
use Webmozart\Assert\Assert;

class CommonSitemapBuilder
{

    /** @var SitemapSource[] */
    private array $sources = [];

    private string $locale = '';

    public function setLocale(string $locale): self
    {
        Assert::inArray($locale, LaravelLocalization::getSupportedLanguagesKeys());
        $this->locale = $locale;
        return $this;
    }

    public function addSource(SitemapSource $source): static
    {
        $this->sources[] = $source;
        return $this;
    }

    public function generate(): void
    {
        Assert::notEmpty($this->locale);

        $indexItems = [];
        foreach ($this->sources as $source) {
            $indexItems = array_merge($indexItems, $source->generate($this->locale));
        }

        $indexContent = View::make('frontend.sitemap.index', ['indexGenerateData' => $indexItems])->render();
        Cache::set("sitemap-{$this->locale}.xml", $indexContent);
    }

    public function generateQuick(): void
    {
        Assert::notEmpty($this->locale);

        $indexItems = [];
        foreach ($this->sources as $source) {
            $indexItems = array_merge($indexItems, method_exists($source, 'generateQuick')
                ? $source->generateQuick($this->locale)
                : $source->generate($this->locale));
        }

        $indexContent = View::make('frontend.sitemap.index', ['indexGenerateData' => $indexItems])->render();

        Cache::set("sitemap-{$this->locale}.xml", $indexContent);
    }

}
