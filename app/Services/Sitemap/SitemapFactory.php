<?php

namespace App\Services\Sitemap;

use App\Services\Sitemap\Sources\PageSource;
use App\Services\Sitemap\Sources\PostSource;
use App\Services\Sitemap\Sources\TagSource;

final class SitemapFactory
{

    public function __construct(
        private CommonSitemapBuilder $sitemap,
        private PageSource $pageSource,
        private PostSource $postSource,
        private TagSource $tagSource
    ) {
    }

    public function createCommonBuilder(): CommonSitemapBuilder
    {
        $this->sitemap
            ->addSource($this->channelSource)
            ->addSource($this->pageSource)
            ->addSource($this->postSource)
            ->addSource($this->tagSource);

        return $this->sitemap;
    }

}
