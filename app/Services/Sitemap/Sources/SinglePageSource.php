<?php

namespace App\Services\Sitemap\Sources;

use App\Services\Sitemap\MapItem;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

abstract class SinglePageSource
{

    protected function generateWithParams(
        array $records,
        string $cacheKey,
        callable $route,
        ?string $updatedAtColumn = 'updated_at',
        string $changeFrequency = MapItem::MONTHLY
    ): bool
    {
        $mapItems = array_map(function (array $record) use ($route, $updatedAtColumn, $changeFrequency) {
            return new MapItem(
                call_user_func($route, $record),
                strtotime($record[$updatedAtColumn]),
                $changeFrequency
            );
        }, $records);

        if (!empty($mapItems)) {
            $xmlContent = View::make('frontend.sitemap.items', ['items' => $mapItems])->render();
            Cache::set($cacheKey, $xmlContent);

            return true;
        } else {
            Cache::delete($cacheKey);

            return false;
        }
    }

}
