<?php

namespace App\Services\Sitemap\Sources;

interface SitemapSource
{

    public function generate(?string $locale = null): array;

}
