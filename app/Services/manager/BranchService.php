<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Branch;
use App\Http\Requests\Branch\{BranchCreateRequest, BranchUpdateRequest};

class BranchService
{
    public function create(BranchCreateRequest $request): Branch
    {
        $model = Branch::create(
            $request->title,
            $request->description,
            $request->address,
            array_filter($request->phone),
            array_filter($request->email),
            (bool)$request->status
        );
        $model->save();

        return $model;
    }

    public function update(BranchUpdateRequest $request, Branch $model): Branch
    {
        $model->edit(
            $request->title,
            $request->description,
            $request->address,
            array_filter($request->phone),
            array_filter($request->email),
            (bool)$request->status
        );

        $model->save();
        return $model;
    }

    public function remove(Branch $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }
}
