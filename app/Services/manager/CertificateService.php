<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Certificate;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Certificate\{CertificateCreateRequest, CertificateUpdateRequest};

class CertificateService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(CertificateCreateRequest $request): Certificate
    {
        $model = Certificate::create(
            $request->title,
            (bool) $request->status,
        );

        if ($request->hasFile('file')) {
            foreach ($request->file as $lang => $file) {
                $file->store('certificates');
                $model->setTranslation('file', $lang, $file->hashName());
            }
        }

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('certificates');
            $model->attachImage($file->hashName());
        }

        $model->save();
        return $model;
    }

    public function update(CertificateUpdateRequest $request, Certificate $model): Certificate
    {
        $model->edit(
            $request->title,
            (bool) $request->status,
        );

        if ($request->hasFile('file')) {
            foreach ($request->file as $lang => $file) {
                $oldFile = $model->getTranslation('file', $lang, false);
                if (Storage::exists('certificates/' . $oldFile)) {
                    Storage::delete('certificates/' . $oldFile);
                }

                $file->store('certificates');
                $model->setTranslation('file', $lang, $file->hashName());
            }
        }

        if ($request->hasFile('image')) {
            if (Storage::exists('certificates/' . $model->image)) {
                Storage::delete('certificates/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('certificates');
            $model->attachImage($file->hashName());
        }

        $model->save();
        return $model;
    }

    public function remove(Certificate $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $pageId)
    {
        /** @var Certificate $model */
        $model = Certificate::findOrFail($pageId);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('certificates/' . $model->image);
        }

    }

    public function removeFile(int $pageId, string $lang)
    {
        /** @var Certificate $model */
        $model = Certificate::findOrFail($pageId);
        $model->setTranslation('file', $lang, null);
        if ($model->save()) {
            Storage::delete('certificates/' . $model->getTranslation('file', $lang, false));
        }
    }
}
