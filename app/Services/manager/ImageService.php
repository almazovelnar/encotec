<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Image;
use App\Http\Requests\Image\ImageRequest;

class ImageService
{
    public function create(ImageRequest $request): Image
    {
        $model = Image::create(
            $request->param,
            (int)$request->photo_id,
            $request->label
        );

        $model->save();

        return $model;
    }

    public function update(ImageRequest $request, Image $model): Image
    {
        $model->edit(
            $request->param,
            (int)$request->photo_id,
            $request->label
        );

        $model->save();
        return $model;
    }

    public function remove(Image $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }
}
