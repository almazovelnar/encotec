<?php

namespace App\Services\manager;

use Cache;
use App\Models\MainInfo;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\MainInfo\MainInfoRequest;

class MainInfoService
{
    public function update(MainInfoRequest $request, MainInfo $model)
    {
        $model->edit(
            $request->title,
            $request->description,
            $request->keywords,
            $request->meta,
            $request->seo,
            $request->address,
            $request->name,
            array_filter($request->email),
            array_filter($request->phone),
            $request->fax,
            $request->location
        );

        if ($model->save()) {
            $this->flushCache();
        }
        return $model;
    }

    public function deleteLogo(int $id, string $lang)
    {
        $model = MainInfo::findOrFail($id);
        Storage::delete('main-info/' . $model->getTranslation('logo', $lang));
        $model->forgetTranslation('logo', $lang);

        if ($model->save()) {
            $this->flushCache();
        }
    }

    public function deleteFavicon(int $id, string $lang)
    {
        $model = MainInfo::findOrFail($id);
        Storage::delete('main-info/' . $model->getTranslation('favicon', $lang));
        $model->forgetTranslation('favicon', $lang);

        if ($model->save()) {
            $this->flushCache();
        }
    }

    public function flushCache(): void
    {
        Cache::delete('site-info');
    }
}
