<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Enum\PageTemplate;
use Str;
use App\Models\Page;
use RuntimeException;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Page\{PageCreateRequest, PageUpdateRequest};

class PageService
{
    public function create(PageCreateRequest $request): Page
    {
        $model = Page::create(
            $request->title,
            $request->description,
            $this->collectMeta($request),
            (bool) $request->status,
            (bool) $request->on_menu,
            $request->template,
            Str::slug($request->slug ?? $request->title[array_key_first($request->title)], '-', 'az')
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('pages');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(PageUpdateRequest $request, Page $model): Page
    {
        $model->edit(
            $request->title,
            $request->description,
            $this->collectMeta($request),
            (bool) $request->status,
            (bool) $request->on_menu,
            Str::slug($request->slug ?? $request->title[array_key_first($request->title)], '-', 'az')
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('pages/' . $model->image)) {
                Storage::delete('pages/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('pages');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    private function collectMeta($request)
    {
        $newMeta = [];
        foreach ($request->meta as $lang => $meta) {
            $newMeta[$lang]['title'] = $meta['title'] ?? $request->title[$lang];
            $newMeta[$lang]['description'] = $meta['description'] ?? ($request->description[$lang] ? Str::words(strip_tags($request->description[$lang])) : '');
        }

        return $newMeta;
    }

    public function remove(Page $model)
    {
        if (($model->template == PageTemplate::MAIN) || !$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $pageId)
    {
        /** @var Page $model */
        $model = Page::findOrFail($pageId);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('pages/' . $model->image);
        }

    }
}
