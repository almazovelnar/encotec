<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Partner;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Partner\{PartnerCreateRequest, PartnerUpdateRequest};

class PartnerService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(PartnerCreateRequest $request): Partner
    {
        $model = Partner::create(
            $request->title,
            $request->link,
            (bool) $request->status,
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('partners');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(PartnerUpdateRequest $request, Partner $model): Partner
    {
        $model->edit(
            $request->title,
            $request->link,
            (bool) $request->status,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('partners/' . $model->image)) {
                Storage::delete('partners/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('partners');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function remove(Partner $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $pageId)
    {
        /** @var Partner $model */
        $model = Partner::findOrFail($pageId);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('partners/' . $model->image);
        }

    }
}
