<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Helpers\CodeGenerator;
use App\Models\Photo;
use App\Models\Folder;
use Illuminate\Http\Request;
use Storage;

class PhotoService
{
    const DISK_NAME = 'photos';

    public function create(Request $request)
    {
        $infoImage = null;
        $storage = \Storage::disk(self::DISK_NAME);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $author = \Auth::user();

            $model = Photo::create(
                $file->hashName(),
                $file->getClientOriginalName(),
                $author->id
            );

            $folder = Folder::find((int)$request->folderId);
            $model->path = $folder->slug ?? null;
            $saveFolder = $model->path;

            if (!$storage->has($saveFolder)) {
                $storage->createDir($saveFolder);
            }

            if ($storage->putFile($saveFolder, $file)) {
                $infoImage = @exif_read_data($storage->path($saveFolder . '/' . $file->hashName()));

                if ($infoImage) {
                    $model->description = $infoImage['ImageDescription'] ?? null;
                }
            }

            if ($model->save()) {
                $model->update(['code' => CodeGenerator::photo($model->id)]);

            }

            return $model;
        }

        return false;
    }

    public function update(Request $request, Photo $photo)
    {
        $photo->edit($request->title, $request->description, (bool)$request->is_exclusive, $request->published_at, $request->author);
        $photo->save();
    }

    public function createPhotoBySize($oldFolder, $oldPath, $filename)
    {
        ini_set('memory_limit', '512M');
        $storagePhotos = Storage::disk(self::DISK_NAME);
        $storage = \Storage::disk('public');

        if ($storagePhotos->exists($oldPath)) {
            foreach ($this->getPhotoSizes() as $photoFolderName => $photoSizes) {
                $image = \Image::make($storagePhotos->path($oldPath));
                $folder = "posts/{$photoFolderName}/{$oldFolder}";
                if (!$storage->has($folder)) {
                    $storage->createDir($folder);
                }
                $path = $storage->path($folder . '/' . $filename);
                $width = null;
                $height = null;

                if ($photoSizes['commonSize']) {
                    if ($image->getHeight() > $image->getWidth()) {
                        $height = $photoSizes['commonSize'];
                        $watermark = 'watermarkH.png';
                    } else {
                        $width = $photoSizes['commonSize'];
                        $watermark = 'watermark.png';
                    }
                } else {
                    if ($image->getHeight() > $image->getWidth()) {
                        $height = $photoSizes['height'];
                        $watermark = 'watermarkH.png';
                    } else {
                        $width = $photoSizes['width'];
                        $height = $photoSizes['height'];
                        $watermark = 'watermark.png';
                    }
                }

                $newImage = $image->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });

                if ($photoSizes['watermark']) {
                    $newImage = $newImage->insert(public_path('images/' . $watermark), 'center');
                }

                $newImage->save($path, 100);
            }
        }
    }

    public function getPhotoSizes()
    {
        return
            [
                'list'  => ['height' => 88, 'width' => 160, 'watermark' => false, 'commonSize' => ''],
                'large' => ['height' => 310, 'width' => 553, 'watermark' => false, 'commonSize' => ''],
            ];
    }

    public function crop(Request $request, Photo $photo)
    {
        $x = (int)$request->x;
        $y = (int)$request->y;
        $x = $x > 0 ? $x : 0;
        $y = $y > 0 ? $y : 0;
        $width = (int)$request->width;
        $height = (int)$request->height;

        \Image::make(public_path('storage/photos/' . $photo->filename))->crop($width, $height, $x, $y)->save();
    }

    public function remove(Photo $photo)
    {
        try {
            $photo->image->delete();
            $photo->forceDelete();
//            \Storage::disk(self::DISK_NAME)->delete(($photo->path ? $photo->path . '/' : '') . $photo->filename);
        } catch (\Exception $e) {
            \Log::debug($e);
            throw new \Exception($e->getMessage());
        }
    }

    public function softRemove(Photo $photo)
    {
        $photo->delete();
    }

    public function restore(Photo $photo)
    {
        $photo->restore();
    }

    public function createFolder(Request $request)
    {
        if (!Folder::query()->where('slug', $request->slug)->first()) {
            $model = Folder::create($request->title, $request->slug);

            if ($model->save()) {
                \Storage::disk(self::DISK_NAME)->createDir($model->slug);
                return $model;
            }
        }

        return false;
    }

    public function updateFolder(Request $request)
    {
        $model = Folder::find($request->folderId);
        $model->edit($request->title);

        if ($model->save()) {
            \Storage::disk(self::DISK_NAME)->createDir($model->slug);
            return $model;
        }
        return false;
    }

    public function deleteFolder(Request $request)
    {
        $folder = Folder::find($request->input('id'));
        $folderPhoto = Photo::query()->where('path', $folder->slug)->first();

        if (!$folderPhoto) {
            Folder::find($request->input('id'))->delete();
//            \Storage::disk(self::DISK_NAME)->deleteDir($folder->slug);
        } else {
            throw new \Exception('This folder have photos');
        }
    }
}
