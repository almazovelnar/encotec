<?php
declare(strict_types=1);

namespace App\Services\manager\Post;

use Str;
use RuntimeException;
use App\Models\Post\{Post, PostPhoto};
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Post\{PostCreateRequest, PostUpdateRequest};

class PostService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(PostCreateRequest $request): Post
    {
        $model = Post::create(
            $request->title,
            $request->description,
            $request->info,
            $this->generateSlug($request->slug, $request->title),
            (bool) $request->status
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('posts');
            $model->attachImage($file->hashName());
        }

        if ($request->hasFile('background')) {
            $file = $request->file('background');
            $file->store('posts');
            $model->attachBackground($file->hashName());
        }

        if ($model->save()) {
            if ($request->photos) {
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = PostPhoto::create($model->id, (int)$photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }
        }

        return $model;
    }

    public function update(PostUpdateRequest $request, Post $model): Post
    {
        $model->edit(
            $request->title,
            $request->description,
            $request->info,
            $this->generateSlug($request->slug, $request->title, $model->id),
            (bool) $request->status,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('posts/' . $model->image)) {
                Storage::delete('posts/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('posts');
            $model->attachImage($file->hashName());
        }

        if ($request->hasFile('icon')) {
            if (Storage::exists('posts/' . $model->background)) {
                Storage::delete('posts/' . $model->background);
                $model->detachBackground();
            }

            $file = $request->file('background');
            $file->store('posts');
            $model->attachBackground($file->hashName());
        }

        if ($model->save()) {
            if ($request->photos) {
                $model->photos()->detach();
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = PostPhoto::create($model->id, $photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }
        }

        return $model;
    }

    public function remove(Post $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $id)
    {
        /** @var Post $model */
        $model = Post::findOrFail($id);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('posts/' . $model->image);
        }
    }

    public function removeBackground(int $id)
    {
        /** @var Post $model */
        $model = Post::findOrFail($id);
        $model->detachBackground();
        if ($model->save()) {
            Storage::delete('posts/' . $model->background);
        }
    }


    private function generateSlug($slugArray, $titleArray, $id = null)
    {
        $newSlug = [];
        foreach ($slugArray as $lang => $slug) {
            $slug = $slug ?? $titleArray[$lang];
            $serviceQuery = Post::query();
            $query = $serviceQuery->where(\DB::raw('slug->"$.' . $lang . '"'), $slug = Str::slug($slug, '-', $lang));
            if ($id) {
                $query->where('id', '!=', $id);
            }

            if ($query->exists()) {
                $id = $serviceQuery
                    ->where(\DB::raw('slug->"$.' . $lang . '"'), $slug)
                    ->latest('id')->value('id');

                $newSlug[$lang]  = "{$slug}-{$id}";
            } else {
                $newSlug[$lang]  = $slug;
            }

        }

        return $newSlug;
    }
}
