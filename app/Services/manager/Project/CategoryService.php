<?php
declare(strict_types=1);

namespace App\Services\manager\Project;

use Str;
use RuntimeException;
use App\Models\Project\Category;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Project\{CategoryCreateRequest, CategoryUpdateRequest};

class CategoryService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(CategoryCreateRequest $request): Category
    {
        $model = Category::create(
            $request->title,
            $request->description,
            $this->generateSlug($request->slug, $request->title),
            (bool) $request->status
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('projects');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(CategoryUpdateRequest $request, Category $model): Category
    {
        $model->edit(
            $request->title,
            $request->description,
            $this->generateSlug($request->slug, $request->title, $model->id),
            (bool) $request->status
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('projects/' . $model->image)) {
                Storage::delete('projects/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('projects');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function remove(Category $model) {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $pageId)
    {
        /** @var Category $model */
        $model = Category::findOrFail($pageId);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('projects/' . $model->image);
        }

    }

    private function generateSlug($slugArray, $titleArray, $id = null)
    {
        $newSlug = [];
        foreach ($slugArray as $lang => $slug) {
            $slug = $slug ?? $titleArray[$lang];
            $serviceQuery = Category::query();
            $query = $serviceQuery->where(\DB::raw('slug->"$.' . $lang . '"'), $slug = Str::slug($slug, '-', $lang));
            if ($id) {
                $query->where('id', '!=', $id);
            }

            if ($query->exists()) {
                $id = $serviceQuery
                    ->where(\DB::raw('slug->"$.' . $lang . '"'), $slug)
                    ->latest('id')->value('id');

                $newSlug[$lang]  = "{$slug}-{$id}";
            } else {
                $newSlug[$lang]  = $slug;
            }

        }

        return $newSlug;
    }
}
