<?php
declare(strict_types=1);

namespace App\Services\manager\Project;

use Str;
use RuntimeException;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Models\Project\{Project, ProjectPhoto};
use App\Http\Requests\Project\{ProjectCreateRequest, ProjectUpdateRequest};

class ProjectService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(ProjectCreateRequest $request): Project
    {
        $model = Project::create(
            $request->category_id,
            $request->title,
            $request->description,
            $this->generateSlug($request->slug, $request->title),
            (bool) $request->status
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('projects');
            $model->attachImage($file->hashName());
        }

        if ($model->save()) {
            if ($request->photos) {
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = ProjectPhoto::create($model->id, (int)$photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }
        }

        return $model;
    }

    public function update(ProjectUpdateRequest $request, Project $model): Project
    {
        $model->edit(
            $request->category_id,
            $request->title,
            $request->description,
            $this->generateSlug($request->slug, $request->title, $model->id),
            (bool) $request->status,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('projects/' . $model->image)) {
                Storage::delete('projects/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('projects');
            $model->attachImage($file->hashName());
        }

        if ($model->save()) {
            if ($request->photos) {
                $model->photos()->detach();
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = ProjectPhoto::create($model->id, $photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }
        }

        return $model;
    }

    public function remove(Project $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $id)
    {
        /** @var Project $model */
        $model = Project::findOrFail($id);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('projects/' . $model->image);
        }
    }

    private function generateSlug($slugArray, $titleArray, $id = null)
    {
        $newSlug = [];
        foreach ($slugArray as $lang => $slug) {
            $slug = $slug ?? $titleArray[$lang];
            $serviceQuery = Project::query();
            $query = $serviceQuery->where(\DB::raw('slug->"$.' . $lang . '"'), $slug = Str::slug($slug, '-', $lang));
            if ($id) {
                $query->where('id', '!=', $id);
            }

            if ($query->exists()) {
                $id = $serviceQuery
                    ->where(\DB::raw('slug->"$.' . $lang . '"'), $slug)
                    ->latest('id')->value('id');

                $newSlug[$lang]  = "{$slug}-{$id}";
            } else {
                $newSlug[$lang]  = $slug;
            }

        }

        return $newSlug;
    }
}
