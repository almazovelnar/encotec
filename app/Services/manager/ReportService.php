<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Report;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Report\{ReportCreateRequest, ReportUpdateRequest};

class ReportService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(ReportCreateRequest $request): Report
    {
        $model = Report::create(
            $request->title,
            (bool) $request->status,
        );

        if ($request->hasFile('file')) {
            foreach ($request->file as $lang => $file) {
                $file->store('reports');
                $model->setTranslation('file', $lang, $file->hashName());
            }
        }

        $model->save();
        return $model;
    }

    public function update(ReportUpdateRequest $request, Report $model): Report
    {
        $model->edit(
            $request->title,
            (bool) $request->status,
        );

        if ($request->hasFile('file')) {
            foreach ($request->file as $lang => $file) {
                $oldFile = $model->getTranslation('file', $lang, false);
                if (Storage::exists('reports/' . $oldFile)) {
                    Storage::delete('reports/' . $oldFile);
                }

                $file->store('reports');
                $model->setTranslation('file', $lang, $file->hashName());
            }
        }

        $model->save();
        return $model;
    }

    public function remove(Report $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeFile(int $pageId, string $lang)
    {
        /** @var Report $model */
        $model = Report::findOrFail($pageId);
        $model->setTranslation('file', $lang, null);
        if ($model->save()) {
            Storage::delete('reports/' . $model->getTranslation('file', $lang, false));
        }
    }
}
