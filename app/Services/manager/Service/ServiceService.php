<?php
declare(strict_types=1);

namespace App\Services\manager\Service;

use Str;
use RuntimeException;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Models\Service\{Service, ServicePhoto};
use App\Http\Requests\Service\{ServiceCreateRequest, ServiceUpdateRequest};

class ServiceService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(ServiceCreateRequest $request): Service
    {
        $model = Service::create(
            $request->title,
            $request->description,
            $this->generateSlug($request->slug, $request->title),
            (bool) $request->status
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('services');
            $model->attachImage($file->hashName());
        }

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            $file->store('services');
            $model->attachIcon($file->hashName());
        }

        if ($model->save()) {
            if ($request->photos) {
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = ServicePhoto::create($model->id, (int)$photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }
        }

        return $model;
    }

    public function update(ServiceUpdateRequest $request, Service $model): Service
    {
        $model->edit(
            $request->title,
            $request->description,
            $this->generateSlug($request->slug, $request->title, $model->id),
            (bool) $request->status,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('services/' . $model->image)) {
                Storage::delete('services/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('services');
            $model->attachImage($file->hashName());
        }

        if ($request->hasFile('icon')) {
            if (Storage::exists('services/' . $model->icon)) {
                Storage::delete('services/' . $model->icon);
                $model->detachIcon();
            }

            $file = $request->file('icon');
            $file->store('services');
            $model->attachIcon($file->hashName());
        }

        if ($model->save()) {
            if ($request->photos) {
                $model->photos()->detach();
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = ServicePhoto::create($model->id, $photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }
        }

        return $model;
    }

    public function remove(Service $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $id)
    {
        /** @var Service $model */
        $model = Service::findOrFail($id);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('services/' . $model->image);
        }
    }

    public function removeIcon(int $id)
    {
        /** @var Service $model */
        $model = Service::findOrFail($id);
        $model->detachIcon();
        if ($model->save()) {
            Storage::delete('services/' . $model->icon);
        }
    }


    private function generateSlug($slugArray, $titleArray, $id = null)
    {
        $newSlug = [];
        foreach ($slugArray as $lang => $slug) {
            $slug = $slug ?? $titleArray[$lang];
            $serviceQuery = Service::query();
            $query = $serviceQuery->where(\DB::raw('slug->"$.' . $lang . '"'), $slug = Str::slug($slug, '-', $lang));
            if ($id) {
                $query->where('id', '!=', $id);
            }

            if ($query->exists()) {
                $id = $serviceQuery
                    ->where(\DB::raw('slug->"$.' . $lang . '"'), $slug)
                    ->latest('id')->value('id');

                $newSlug[$lang]  = "{$slug}-{$id}";
            } else {
                $newSlug[$lang]  = $slug;
            }

        }

        return $newSlug;
    }
}
