<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Models\Slide;
use RuntimeException;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Slide\{SlideCreateRequest, SlideUpdateRequest};

class SlideService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(SlideCreateRequest $request): Slide
    {
        $model = Slide::create(
            $request->title,
            $request->description,
            $request->link,
            (bool) $request->status,
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('slides');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(SlideUpdateRequest $request, Slide $model): Slide
    {
        $model->edit(
            $request->title,
            $request->description,
            $request->link,
            (bool) $request->status,
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('slides/' . $model->image)) {
                Storage::delete('slides/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('slides');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function remove(Slide $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $pageId)
    {
        /** @var Slide $model */
        $model = Slide::findOrFail($pageId);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('slides/' . $model->image);
        }

    }
}
