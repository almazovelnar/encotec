<?php

namespace App\Services\manager;

use App\Models\Post;
use App\Models\PostTag;
use App\Models\Tag;
use App\Helpers\StringHelper;
use App\Services\CacheableDataService;
use App\Http\Requests\Tag\{TagCreateRequest, TagUpdateRequest};

class TagService
{
    public function __construct(private CacheableDataService $dataService)
    {
    }

    public function create(TagCreateRequest $request)
    {
        $model = Tag::create(
            $request->name,
            StringHelper::generateSlug($request->slug ?? $request->name, $request->language),
            $request->language,
            $request->description,
            0,
            (bool)$request->chosen
        );

        if ($model->save()) {
            $this->flushCache($model);
        }

        return $model;
    }

    public function update(TagUpdateRequest $request, $model)
    {
        /** @var Tag $model */
        $model->edit(
            $request->name,
            StringHelper::generateSlug($request->slug ?? $request->name, $model->language),
            $request->description,
            (bool)$request->chosen
        );

        if ($model->save()) {
            $this->flushCache($model);
        }
        return $model;
    }

    public function remove($model) {
        $this->flushCache($model);
        $model->delete();
    }

    public function merge() {
        $tags = \DB::table('tags')
            ->selectRaw('slug, COUNT(id) as countSame')
            ->having('countSame', '>', 1)
            ->groupBy('slug')
            ->get()
            ->take(50)
            ->toArray();

        foreach ($tags as $tag) {
            $tagsBySlug = Tag::query()->where('slug', $tag->slug)->orderByDesc('count')->get();
            $usedCount = 0;
            $idsForDelete = [];
            $newTag = null;

            foreach ($tagsBySlug as $key => $tagBySlug) {
                $usedCount += $tagBySlug->count;
                if ($key != 0) {
                    $idsForDelete[] = $tagBySlug->id;
                }
                if ($key == 0) {
                    $newTag = $tagBySlug;
                }

                $this->flushCache($tagBySlug);
            }

            if ($newTag) {
                PostTag::query()->where('tag_id', $idsForDelete)->update(['tag_id' => $newTag->id]);
                $newTag->update(['count' => $usedCount]);
                Tag::query()
                    ->where('id', $idsForDelete)
                    ->where('id', '!=', $tagsBySlug->get(0)->id)
                    ->delete();
            }
        }

        return count($tags);
    }

    protected function flushCache(Tag $tag)
    {
        $this->dataService->deleteTagBySlugCache($tag->slug);
        $this->dataService->deletePopularTagsCache($tag->language);
        $this->dataService->deleteChosenTagsCache($tag->language);
        $this->dataService->deletePostsByTagCache($tag->id);
        foreach ($tag->posts as $post) {
            $this->dataService->deletePostTagsCache($post->id);
        }
    }
}
