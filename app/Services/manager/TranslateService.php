<?php

namespace App\Services\manager;

use App\Models\Translate;
use App\Http\Requests\Translate\TranslateRequest;

class TranslateService
{
    public function create(TranslateRequest $request) {

        $model = Translate::create($request->group, $request->key, $request->text);
        $model->save();

        return $model;
    }

    public function update(TranslateRequest $request, Translate $model)
    {
        $model->edit($request->group, $request->key, $request->text);
        $model->save();

        return $model;
    }

    public function remove(Translate $translate)
    {
        $translate->delete($translate);
    }
}
