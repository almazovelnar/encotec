<?php

namespace App\Services\manager;

use App\Enum\Status;
use App\Models\User;
use Auth;
use Exception;
use Grpc\Call;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\User\{UserCreateRequest, UserUpdateRequest};

class UserService
{
    public function create(UserCreateRequest $request)
    {
        $model = User::create(
            $request->name,
            $request->email,
            (bool)$request->status,
            $request->password
        );

        $model->assignRole($request->role);

        if ($request->hasFile('thumb')) {
            $file = $request->file('thumb');
            $file->store('users');
            $model->attachThumb($file->hashName());
        }

        $model->save();
        $this->flushCache();
        return $model;
    }

    public function update(UserUpdateRequest $request, $model)
    {
        /** @var User $model */
        $model->edit(
            $request->name,
            $request->email,
            $request->password
        );

        if ($request->role) {
            $model->syncRoles($request->role);
        }

        if ($request->hasFile('thumb')) {
            if (Storage::exists('users/' . $model->thumb)) {
                Storage::delete('users/' . $model->thumb);
                $model->detachThumb();
            }

            $file = $request->file('thumb');
            $file->store('users');
            $model->attachThumb($file->hashName());
        }

        if (User::where('status', Status::ACTIVE)->where('id', '<>', $model->id)->count() > 0) {
            $model->status = (bool)$request->status;
        }

        if ($model->id == Auth::id() && $model->status == false) {
            Auth::guard()->logout();
        }

        if ($model->isDirty('group_id') && $model->save()) {
            $this->flushCache();
        }

        return $model;
    }

    public function remove($model) {
        if (User::where('status', Status::ACTIVE)->where('id', '<>', $model->id)->count() > 0) {
            $this->deleteThumb($model->id);
            $model->delete();
        } else {
            throw new Exception(trans('admin.form.cant_remove_this_user'));
        }
    }

    public function deleteThumb(int $id)
    {
        $model = User::findOrFail($id);
        Storage::delete('users/' . $model->thumb);
        $model->detachThumb();
        $model->save();
    }

    private function flushCache()
    {
        \Artisan::call('app:fix');
    }
}
