<?php

namespace App\Traits;

use App\Enum\ActionType;

trait SlugGenerate
{

    public function generateSlug(string $slug, ?int $id = null, ?string $type = ActionType::CREATE): string
    {
        $query = self::query()->where('slug', $slug);
        if ($type == ActionType::UPDATE) {
            $query->where('id', '!=', $id);
        } else {
            $id = self::query()->first() ? self::query()->first()->id : null;
        }
        if ($query->exists()) {
            return $slug . $id ? ('-' . $id * 11) : '';
        }
        return $slug;
    }

}

