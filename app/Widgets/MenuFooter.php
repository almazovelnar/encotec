<?php

namespace App\Widgets;

use App\Repositories\BranchRepository;
use Arrilot\Widgets\AbstractWidget;
use App\Repositories\PageRepository;

class MenuFooter extends AbstractWidget
{
    protected $config = [];

    public function __construct(
        private PageRepository   $pageRepository,
        private BranchRepository $branchRepository
    )
    {
        parent::__construct();
    }


    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.menu-footer', [
            'config'   => $this->config,
            'pages'    => $this->pageRepository->getShowedItems(),
            'branches' => $this->branchRepository->all(2)
        ]);
    }
}
