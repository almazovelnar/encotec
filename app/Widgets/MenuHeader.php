<?php

namespace App\Widgets;

use App\Repositories\PageRepository;
use Arrilot\Widgets\AbstractWidget;

class MenuHeader extends AbstractWidget
{
    protected $config = [];

    public function __construct(private PageRepository $pageRepository)
    {
        parent::__construct();
    }

    public function run()
    {
        return view('widgets.menu-header', [
            'config' => $this->config,
            'pages'  => $this->pageRepository->getMenuItems(),
        ]);
    }
}
