<?php

namespace App\Widgets\PhotoManager;

use App\Models\Folder;
use Arrilot\Widgets\AbstractWidget;

class Photo extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $modal = \View::make('widgets.photo_manager._modals', [
            'mode' => 'single',
            'folders' => Folder::query()->latest()->get()
        ]);

        return view('widgets.photo_manager.single', [
            'config' => $this->config,
            'modal' => $modal,
            'photoId' => $this->config['photo_id'] ?? null,
            'filename' => $this->config['filename'] ?? null,
            'path' => $this->config['path'] ?? null,
        ]);
    }
}
