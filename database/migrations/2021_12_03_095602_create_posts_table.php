<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->json('title');
            $table->json('description')->nullable();
            $table->json('info')->nullable();
            $table->json('slug');
            $table->string('image')->nullable();
            $table->string('background')->nullable();
            $table->boolean('status')->default(\App\Enum\Status::ACTIVE);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('category_id', 'fk_post_categories_posts_category_id')
                ->on('post_categories')
                ->references('id')
                ->restrictOnDelete();

            $table->index('published_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
