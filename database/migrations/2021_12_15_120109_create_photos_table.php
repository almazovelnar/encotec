<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('author_id');
            $table->json('title')->nullable();
            $table->json('description')->nullable();
            $table->string('code', 12)->nullable();
            $table->string('slug')->nullable();
            $table->string('filename')->unique();
            $table->string('original_filename');
            $table->string('path')->nullable();
            $table->char('resolution', 11)->nullable();
            $table->json('information')->nullable();
            $table->json('source')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('author_id', 'author_photo_key')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
