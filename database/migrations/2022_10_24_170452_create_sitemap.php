<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitemap extends Migration
{

    public function up()
    {
        Schema::create('sitemap', function (Blueprint $table) {
            $table->unsignedInteger('id', true);

            $table->unsignedBigInteger('post_id');
            $table->enum('language', ['az', 'ru']);
            $table->unsignedSmallInteger('page');
            $table->string('link', 255);
            $table->dateTime('last_mod');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate();

            $table->unique(['post_id', 'page', 'language']);
            $table->index(['page', 'language']);

            $table->foreign('post_id')
                ->on('posts')
                ->references('id')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sitemap');
    }
}
