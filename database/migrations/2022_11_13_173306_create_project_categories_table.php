<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

class CreateProjectCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_categories', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger(NestedSet::LFT)->default(0);
            $table->unsignedInteger(NestedSet::RGT)->default(0);
            $table->unsignedBigInteger(NestedSet::PARENT_ID)->nullable();

            $table->json('title');
            $table->json('description')->nullable();
            $table->json('slug')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();

            $table->index(NestedSet::getDefaultColumns());
            $table->foreign('parent_id', 'project_category_parent_key')->references('id')->on('project_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_categories');
    }
}
