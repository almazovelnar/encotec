<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->json('title');
            $table->json('description')->nullable();
            $table->json('slug')->nullable();
            $table->string('image')->nullable();
            $table->boolean('status');
            $table->dateTime('date')->nullable();
            $table->timestamps();

            $table->foreign('category_id', 'fk_project_categories_projects_category_id')
                ->on('project_categories')
                ->references('id')
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
