<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = $this->getConfigList();

        foreach ($list as $key => $item) {
            if (Config::query()->where('param', $key)->exists()) {
                continue;
            }

            $model = new Config();
            $model->param = $key;
            $model->value = '';
            $model->default = '#';
            $model->label = $item;
            $model->type = 'string';

            $model->save();
        }
    }

    public function getConfigList()
    {
        return [
            'FACEBOOK'                 => 'Facebook profile link',
            'YOUTUBE'                  => 'Youtube profile link',
            'TWITTER'                  => 'Twitter profile link',
            'LINKEDIN'                 => 'Linkedin profile link',
            'CAREER_EMAIL'             => 'Career email for sending CV',
            'REQUEST_PHONE'            => 'Phone number at footer',
            'COMPLETED_PROJECTS_COUNT' => 'Home page completed projects count',
            'PERSONAL_COUNT'           => 'Home page personal count count',
            'EXPERIENCE_YEARS_COUNT'   => 'Home page experience years count',
            'CUSTOMERS_COUNT'          => 'Home page customers count',
        ];
    }
}
