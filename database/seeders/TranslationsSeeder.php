<?php

namespace Database\Seeders;

use Arr;
use App\Models\Translate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->getTranslations() as $group => $translates) {
            foreach ($translates as $prefix => $translations) {
                $withPrefix = Arr::isAssoc($translations);
                if (!$withPrefix) {
                    $translations = [$translations];
                }

                foreach ($translations as $key => $text) {
                    $translate = DB::table('language_lines')
                        ->where('group', $group)
                        ->where('key', $withPrefix ? "$prefix.$key" : $prefix);


                    $text = json_encode([
                        'az' => $text[0],
                        'ru' => $text[1]
                    ]);

                    if (!$translate->first()) {
                        $translate->insert([
                            'group' => $group,
                            'key'   => $withPrefix ? "$prefix.$key" : $prefix,
                            'text'  => $text
                        ]);
                    }

                }
            }
        }

        // clear cache
        Translate::first()->save();
    }

    private function getTranslations(): array
    {
        return [
            'site' => [
                'layout' => [
                    'template' => ['', '', ''],

                    'footer_text'             => ['Footer text', 'Footer text', 'Footer text'],
                    'quick_links'             => ['Linklər', 'Quick Links', 'Quick Links'],
                    'company_address'         => ['Ünvan', 'Address', 'Address'],
                    'company_phones'          => ['Əlaqə nömrəsi', 'Phone', 'Phone'],
                    'company_emails'          => ['E-mail', 'Email', 'Email'],
                    'all_rights_reserved'     => ['Bütün hüquqlar qorunur :year', 'Все права защищены :year', 'Все права защищены :year'],
                    '404_not_found'           => ['Səhifə tapılmadı', 'Страница не найдена', 'Страница не найдена'],
                    'career_section_title'    => ['Karyera', 'Career', 'Career'],
                    'career_section_subtitle' => ['Cv-nizi bizə göndərin', 'Send your cv', 'Send your cv'],
                    'career_text'             => [
                        'Komandamıza qoşulmaq üçün CV-nizi :email e-poçt ünvanına göndərə bilərsiniz',
                        'You can send your CV to the following e-mail address :email to join our team',
                        'You can send your CV to the following e-mail address :email to join our team'
                    ],
                    'learn_more'              => ['Ətraflı', 'Learn More', 'Learn More'],
                    'career'                    => ['Karyera', 'Career', 'Career'],
                    'contacts'                  => ['Əlaqə', 'Contacts', 'Contacts'],
                ],
                'home'   => [
                    'template' => ['', '', ''],

                    'about_us_section_title'    => ['Biz kimik', 'Who we are', 'Who we are'],
                    'about_us_section_subtitle' => ['Haqqımızda', 'About us', 'About us'],
                    'about_us'                  => ['Haqqımızda', 'Haqqımızda', 'Haqqımızda'],
                    'clients'                   => ['Müştərilər', 'Clients', 'Clients'],
                    'certificates'              => ['Sertifikatlar', 'Certificates', 'Certificates'],
                    'reports'                   => ['Audit Hesabatları', 'Audit Reports', 'Audit Reports'],
                    'services_section_title'    => ['Biz nə edirik', 'What we do', 'What we do'],
                    'services_section_subtitle' => ['Xidmətlər', 'Services', 'Services'],
                    'projects_section_title'    => ['Seçilmiş layihələr', 'Selected projects', 'Selected projects'],
                    'projects_section_subtitle' => ['Bizim işlərimiz', 'Our works', 'Our works'],
                    'clients_section_title'     => ['Müştərilərimiz', 'Our clients', 'Our clients'],
                    'clients_section_subtitle'  => ['Artım üçün tərəfdaşlıq', 'Partnershi̇p for growth', 'Partnershi̇p for growth'],
                    'news_section_title'        => ['Son xəbərlər', 'Latest news', 'Latest news'],
                    'news_section_subtitle'     => ['Xəbərlər', 'News', 'News'],
                ],
                'statistics' => [
                    'completed_projects'        => ['Bitmiş layihələr', 'Completed Projects', 'Completed Projects'],
                    'personal'                  => ['Əməkdaşlar', 'Personal', 'Personal'],
                    'year_of_experience'        => ['Təcrübə ili', 'Year Of Experience', 'Year Of Experience'],
                    'customers'                 => ['Müştərilər', 'Customers', 'Customers'],
                ],
                'page'   => [
                    'shorts' => ['Shorts', 'Shorts', 'Shorts'],
                ],
            ],
        ];
    }
}
