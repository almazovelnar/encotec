<?php

use Carbon\Carbon;
use App\Components\Editable;
use App\Components\Config;
use App\Components\Grid\Grid;
use App\Components\Information;

if (!function_exists('grid')) {
    function grid($config)
    {
        return (new Grid($config))->render();
    }
}

if (!function_exists('site_info')) {
    function site_info(string $key, $default = null)
    {
        return Information::instance()->get($key, $default);
    }
}

if (!function_exists('site_config')) {
    function site_config(string $key)
    {
        return Config::instance()->get($key);
    }
}

if (!function_exists('album_cover')) {
    function album_cover($filename): string
    {
        if (empty($filename)) {
            return asset('images/thunk.svg');
        }
        return asset('storage/posts/' . $filename);
    }
}

if (!function_exists('getParsedDate')) {
    function getParsedDate($date, string $format = 'd F Y'): string
    {
        if (is_a($date, Carbon::class)) {
            return $date->translatedFormat($format);
        }
        return Carbon::parse($date)->translatedFormat($format);
    }
}

if (!function_exists('get_image')) {
    function get_image($filename, $folder = '', ?string $size = null): string
    {
        $storageOriginal = Storage::disk('photos');
        $storage  = Storage::disk('public');

        if (empty($filename)) {
            return asset('images/thunk.svg');
        }

        $original = $storageOriginal->path(($folder ? $folder . '/' : '') . $filename);

        if (!$size) {
            return asset('storage/photos/' . ($folder ?  $folder . '/' : '') . $filename);
        }

        $sizes    = config('image-sizes.template');
        $path     = $storage->path('thumbs/' . $size . ($folder ?  '/' . $folder . '/' : '/'));

        $width = $sizes[$size]['w'];
        $height = $sizes[$size]['h'];

        if (!is_file($original)) {
            return asset('images/thunk.svg');
        }

        if (!is_file($path . $filename)) {
            ini_set('memory_limit', '512M');

            if (!is_dir($path)) {
                $storage->createDir('thumbs/' . $size . '/' . $folder);
            }

            try {
                $image = \Intervention\Image\Facades\Image::make($original);
                if ($image->getHeight() > $image->getWidth()) {
                    $width = $sizes[$size]['h'];
                    $height = $sizes[$size]['w'];
                }
                \Intervention\Image\Facades\Image::make($original)
                    ->fit($width, $height, function ($constraint) {
                        $constraint->upsize();
                    })
                    ->save($path . $filename);
            } catch (\Exception $exception) {
                return asset('images/thunk.svg');
            }
        }

        return asset('storage/thumbs/' . ($size == 'o' ? '' : $size . '/') . ($folder ?  $folder . '/' : '') . $filename);
    }

}

if (!function_exists('getPostLangClass')) {
    function getPostLangClass($postId, $group_id, $lang): ?string
    {
        $query = \App\Models\Post::query()
            ->where('language', $lang);
            $query->where('group_id', $group_id);
            $query->where('group_id','!=', null);
        if (!$query->exists()) {
            return 'notHave';
        }
        return null;
    }
}

if (!function_exists('getCategoryLangClass')) {
    function getCategoryLangClass($catId, $group_id, $lang): ?string
    {
        if (!\App\Models\Category::query()
            ->where('group_id', $group_id)
            ->where('language', $lang)->exists()) {

            return 'notHave';
        }

        return null;
    }
}

if (!function_exists('getChannelLangClass')) {
    function getChannelLangClass($id, $group_id, $lang): ?string
    {
        if (!\App\Models\Channel::query()
            ->where('group_id', $group_id)
            ->where('language', $lang)->exists()) {

            return 'notHave';
        }

        return null;
    }
}

if (!function_exists('user_image')) {
    function user_image($filename): string
    {
        if (empty($filename)) {
            return asset('images/user.png');
        }

        return asset('storage/users/' . $filename);
    }
}

if (!function_exists('author_image')) {
    function author_image($filename): string
    {
        if (empty($filename)) {
            return asset('images/user.png');
        }

        return asset('storage/authors/' . $filename);
    }
}

if (!function_exists('get_logo')) {
    function get_logo(): string
    {
        if (empty($logo = site_info('logo'))) {
            return asset('assets/frontend/img/logo.svg');
        }

        return asset('storage/main-info/' . $logo);
    }
}

if (!function_exists('get_favicon')) {
    function get_favicon(): string
    {
        if (empty($favicon = site_info('favicon'))) {
            return asset('assets/frontend/img/favicon.png');
        }

        return asset('storage/main-info/' . $favicon);
    }
}

if (!function_exists('get_colored_title')) {
    function get_colored_title($title, $markedWords = null): string
    {
        if (empty($markedWords)) {
            return $title;
        }

        foreach ($markedWords as $markedWord){
            $title = str_replace($markedWord, "<i class='marked-word'>{$markedWord}</i>", $title);
        }

        return $title;
    }
}

if (!function_exists('get_weather_icon')) {
    function get_weather_icon($filename): string
    {
        return asset("images/weather/$filename.png");
    }
}

if (!function_exists('get_time')) {
    function get_time(): string
    {
        $now = Carbon::parse(now());
        try {
            $parsedDate = explode(' ', $now->translatedFormat('l, d F, H:i'));
            $parsedDate[2] = Str::ucfirst($parsedDate[2]);
            return implode(' ', $parsedDate);
        } catch (Exception $e) {
            return $now->translatedFormat('l, d F, H:i');
        }
    }
}

if (!function_exists('get_video_thumb')) {
    function get_video_thumb(string $code, $quality = '0'): string
    {
        return "https://i.ytimg.com/vi/{$code}/{$quality}.jpg";
    }
}

if (!function_exists('get_video')) {
    function get_video(?string $video): ?string
    {
        return $video ? asset('files/file/' . $video) : NULL;
    }
}

if (!function_exists('editable')) {
    function editable(): Editable
    {
        return Editable::instance();
    }
}




