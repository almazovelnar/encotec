$(document).ready(function () {
    let currentUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

    $(".nav-treeview .nav-link, .nav-link").each(function () {
        if (currentUrl.indexOf($(this).attr('href')) !== -1) {
            $(this).addClass('active');
            $(this).parent().parent().parent().addClass('menu-is-opening menu-open');
        }
    });

    $('#langSelector').change(function () {
        let currentLangValue = $(this).val();
        let w = window.location;
        w.href = w.pathname + '?filters%5Blanguage%5D=' + currentLangValue;
    });

    $('.inputCreator').click(function() {
        let makeType = $(this).data('make-type');
        let makeName = $(this).data('make-name');
        let newElement = $('<input />', {
            type: makeType,
            name: makeName,
            class: 'form-control new-input'
        }).appendTo($('.' + makeType + '-inputs'));
    });

    $(".v-block").on("mouseover", function() {
        $(this).find('video').get(0).play();

    }).on('mouseleave', function() {
        $(this).find('video').get(0).load();
    });

    $('.select-title').on('click', function () {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        text = text.trim();
        if (text.length > 0){

            $('.marked_words').append("<option value='"+text+"' selected>"+text+"</option>");
            $('.marked_words').trigger('change');
        }
    })

    tinymce.init({
        language: 'az',
        plugins: 'table link addWidget addText',
        selector: '.tinymce',
        height: 600,
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link | addWidget | addText',
    	mobile: {
          menubar: true
        }});

    $('.open-menu').on('click', function () {
        $('.main-sidebar').toggleClass('open')
    })


    $('#youtube').on('keyup', function (e) {
        e.preventDefault();
        let id = $(this).val();
        let matches = id.match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#");

        if (id.length >= 7) {
            $('#videoBlock').html('<iframe width="100%" height="300" src="https://www.youtube.com/embed/' + (matches!=null ? matches[0] : id) + '?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')
        } else {
            $('#videoBlock').html('')
        }
    });

    $('.datetimepicker').datetimepicker({
        icons: {time: 'far fa-clock'},
        format: 'YYYY-MM-DD HH:mm:ss',
        pickDate: false,
        pickSeconds: false,
        pick12HourFormat: false,
        ignoreReadonly: true
    });


    $('select:not(#langSelector)').on('change', function () {
        $('#grid_view_filters_form').submit()
    })

    if ($(".file_uploader").length > 0) {
        $(".file_uploader").each(function () {
            let fileUploader = $(this)
            let fileUploaderOptions = {};
            if ((fileUploaderValue = fileUploader.attr('value'))){
                fileUploaderOptions.initialPreview = fileUploaderValue
            }
            fileUploaderOptions.initialPreviewShowDelete = false;
            if ((fileUploaderDeleteThumbUrl = fileUploader.data('delete')) ){
                fileUploaderOptions.deleteUrl = fileUploaderDeleteThumbUrl
                fileUploaderOptions.initialPreviewShowDelete = true;
            }
            fileUploaderOptions.showRemove = false;
            fileUploaderOptions.maxFileCount= 1;
            fileUploaderOptions.initialPreviewDownloadUrl = fileUploaderValue;
            fileUploaderOptions.initialPreviewAsData = true;
            fileUploaderOptions.overwriteInitial = false;
            fileUploaderOptions.showUpload = false;
            fileUploaderOptions.showCancel = false;
            fileUploaderOptions.showClose = false;
            fileUploaderOptions.initialPreviewConfig = [{
                type: fileUploader.data('preview-file-type'),
                filetype: fileUploader.data('preview-file-type')=='video' ? 'video/mp4' : false
            },]

            fileUploader.fileinput(fileUploaderOptions);
        })
    }

    let _alert = $(".alert");
    if (_alert.length) {
        _alert.animate({opacity: 1.0}, 2000).fadeOut("slow");
    }
})
