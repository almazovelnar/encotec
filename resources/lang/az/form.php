<?php

return [
    'success' => [
        'save'      => 'Dəyişiklər yadda saxlanıldı',
        'tag_merge' => ':count teq uğurla birləşdirildi',
        'delete'    => 'Məlumat uğurla silindi',
    ],
    'error'   => [
        'cant_remove_this_user' => 'Bu istifadəçini silmək mümkün olmadı',
        'save'                  => 'Dəyişikləri yadda saxlamaq olmadı',
        'tag_merge'             => 'Teqləri birləşdirmək mümkün olmadı',
        'delete'                => 'Məlumatı silmək mümkün olmadı',
    ]
];
