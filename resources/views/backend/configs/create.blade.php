@extends('backend.layouts.app')

@section('title', trans('admin.new_config'))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.config.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-tabs">
                            <div class="card-header p-0 pt-1 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="param">@lang('admin.config_param')</label>
                                    <input type="text" id="param" class="form-control @error('param') is-invalid @enderror" name="param" value="{{ old('param') }}">
                                    @error('param')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                            <div class="form-group">
                                                <label for="value-{{ $lang }}">@lang('admin.config_value')</label>
                                                <textarea name="value[{{ $lang }}]" id="value-{{ $lang }}" rows="4" class="form-control @error('value.' . $lang) is-invalid @enderror">{{ old('value.' . $lang) }}</textarea>
                                                @error('value.'  . $lang)
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <div class="form-group">
                                    <label for="default">@lang('admin.config_default')</label>
                                    <textarea name="default" id="default" rows="4" class="form-control @error('default') is-invalid @enderror">{{ old('default') }}</textarea>

                                    @error('default')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="label">@lang('admin.config_label')</label>
                                    <input type="text" id="label" class="form-control @error('label') is-invalid @enderror" name="label" value="{{ old('label') }}">
                                    @error('label')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="type">@lang('admin.config_type')</label>
                                    <input type="text" id="type" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ old('type') }}">
                                    @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
