@extends('backend.layouts.app')

@section('title', trans('admin.images'))

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
            'columnFields' => [
                [
                    'label' => trans('admin.image'),
                    'attribute' => 'photo_id',
                    'filter' => false,
                    'value' => function (\App\Models\Image $model) {
                        return "<img src='" . get_image($model->photo->filename,$model->photo->path, 'list') . "' style='max-width:150px; max-height:100px;'>";
                    },
                    'format' => 'html'
                ],
                [
                    'label' => trans('admin.label'),
                    'attribute' => 'label'
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit'
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
