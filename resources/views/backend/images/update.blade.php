@extends('backend.layouts.app')

@section('title', trans('admin.image'))

@section('content')
    <div>
        <form action="{{ route('admin.images.update', $image->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="form-group">
                                    <label for="param">@lang('admin.param')</label>
                                    <input type="text" name="param" id="param" class="form-control @error('param') is-invalid @enderror" value="{{ old('param', $image->param) }}">

                                    @error('param')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="label">@lang('admin.label')</label>
                                    <textarea type="text" name="label" id="label" class="form-control @error('label') is-invalid @enderror">{{ old('label', $image->label) }}</textarea>

                                    @error('label')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="param">@lang('admin.image')</label>
                                @widget('PhotoManager\Photo', [
                                    'photo_id' => $image->photo_id,
                                    'filename' => $image->photo->filename,
                                    'path' => $image->photo->path
                                ])

                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
