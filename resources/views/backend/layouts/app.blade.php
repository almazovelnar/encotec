<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Admin Panel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('backend.layouts.styles')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('admin.home') }}" class="brand-link">
            <img src="{{ asset('images/AdminLogo.png') }}" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">@lang('admin_panel_name')</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ user_image(auth()->user()->thumb) }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="{{ route('admin.users.show', auth()->id()) }}" class="d-block">{{ auth()->user()->full_name }}</a>
                </div>
            </div>

            @include('backend.layouts.menu')

        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('backend.layouts.breadcrumb')

        <button class="open-menu"><img src="/images/menu.svg" alt="icon"></button>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @include('backend.layouts.notification')
                @yield('content')
            </div><!-- /.container-fluid -->
        </section>
    </div>

    @include('backend.layouts.footer')

    @yield('end_load')
</div>
<!-- ./wrapper -->
    @include('backend.layouts.scripts')
</body>
</html>
