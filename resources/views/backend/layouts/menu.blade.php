@php
    /** @var \App\Models\User $user */
    $user = auth()->user();
@endphp
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class  with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ route('admin.slides.index') }}" class="nav-link">
                <i class="nav-icon fas fa-images"></i>
                <p>
                    @lang('admin.menu.slides')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.posts.index') }}" class="nav-link">
                <i class="nav-icon fas fa-newspaper"></i>
                <p>
                    @lang('admin.menu.posts')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="javascript:void(0);" class="nav-link">
                <i class="nav-icon fas fa-clipboard-list"></i>
                <p>
                    @lang('admin.menu.projects')
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('admin.project-categories.index') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.categories')
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.projects.index') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.projects')
                        </p>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.services.index') }}" class="nav-link">
                <i class="nav-icon fas fa-users-cog"></i>
                <p>
                    @lang('admin.menu.services')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.certificates.index') }}" class="nav-link">
                <i class="nav-icon fas fa-certificate"></i>
                <p>
                    @lang('admin.menu.certificates')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.reports.index') }}" class="nav-link">
                <i class="nav-icon fas fa-file-archive"></i>
                <p>
                    @lang('admin.menu.reports')
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.partners.index') }}" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    @lang('admin.menu.partners')
                </p>
            </a>
        </li>

        @if($user->can('pageManage'))
            <li class="nav-item">
                <a href="{{ route('admin.pages.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        @lang('admin.menu.pages')
                    </p>
                </a>
            </li>
        @endif
        <li class="nav-item">
            <a href="{{ route('admin.branches.index') }}" class="nav-link">
                <i class="nav-icon fas fa-map"></i>
                <p>
                    @lang('admin.menu.branches')
                </p>
            </a>
        </li>
        @if($user->can('userManage'))
            <li class="nav-item">
                <a href="{{ route('admin.users.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        @lang('admin.menu.users')
                    </p>
                </a>
            </li>
        @endif

        <li class="nav-item">
            <a href="javascript:void(0);" class="nav-link">
                <i class="nav-icon fas fa-cogs"></i>
                <p>
                    @lang('admin.menu.settings')
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                @if($user->can('mainInfoManage'))
                    <li class="nav-item">
                        <a href="{{ route('admin.main-info') }}" class="nav-link">
                            <p>
                                <i class="nav-icon">-</i>
                                @lang('admin.menu.main-info')
                            </p>
                        </a>
                    </li>
                @endif
                @if($user->can('configManage'))
                    <li class="nav-item">
                        <a href="{{ route('admin.config.index') }}" class="nav-link">
                            <p>
                                <i class="nav-icon">-</i>
                                @lang('admin.menu.configs')
                            </p>
                        </a>
                    </li>
                @endif
                @if($user->can('translateManage'))
                    <li class="nav-item">
                        <a href="{{ route('admin.translates.index') }}" class="nav-link">
                            <p>
                                <i class="nav-icon">-</i>
                                @lang('admin.menu.translates')
                            </p>
                        </a>
                    </li>
                @endif
                <li class="nav-item">
                    <a href="{{ route('admin.images.index') }}" class="nav-link">
                        <p>
                            <i class="nav-icon">-</i>
                            @lang('admin.menu.images')
                        </p>
                    </a>
                </li>
            </ul>
        </li>
    </ul>


    <a data-href="{{ route('admin.logout') }}" id="logout-button">{{ trans('admin.logout') }}</a>
</nav>
