<script src="{{ asset('assets/backend/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('assets/backend/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/jquery.knob.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/adminlte.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/fileinput.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/dropzone-min.js') }}"></script>
<script src="/assets/backend/js/admin.js?v=8.1"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        if ($('.ckEditor').length > 0) {
            $('.ckEditor').each(function () {
                CKEDITOR.replace($(this).prop('id'), {
                    filebrowserUploadUrl: "{{route('admin.ckeupload', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form',
                    height: 400,
                    removeButtons: 'Image'
                });
            });
        }

        $('.delete-btn').click(function () {
            let res = confirm("{{ trans('admin.delete_item_confirm') }}");
            if (!res) return false;

            let url = $(this).data('action');
            let form = $('#delete-form');
            form.attr('action', url).submit()
        });

        $('#logout-button').click(function () {
            let w = window, confirm = w.confirm("{{ trans('admin.logout') }}");
            if (confirm) {
                w.location.href = $(this).data('href');
            }
        });

        $('.tags').select2({
            tags: true,
            allowClear: true,
            minimumInputLength: 3,
            createTag: function (params) {
                var term = $.trim(params.term);

                if (term === '') {
                    return null;
                }

                return {
                    id: term,
                    text: term.replace('Teq yarat: ', ''),
                    newTag: true
                }
            },
            insertTag:function(data, tag) {
                tag.text = 'Teq yarat: ' + tag.text;
                data.push(tag);
            },
            ajax: {
                type: 'GET',
                url: "{{ route('admin.posts.tag-list') }}",
                dataType: 'json',
                data: function (params) {
                    return {lang: $(this).closest('.tab-pane').data('lang'), q: params.term};
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.name,
                                id: item.name
                            }
                        })
                    };
                },
            }
        });
        // $('.marked_words').select2({
        //     formatNoMatches: function() {
        //         return '';
        //     },
        //     tags: true,
        //     allowClear: true,
        //     minimumResultsForSearch: -1
        // });
        {{--$('.translate').select2({--}}
        {{--    tags: false,--}}
        {{--    allowClear: true,--}}
        {{--    minimumInputLength: 3,--}}
        {{--    ajax: {--}}
        {{--        type: 'GET',--}}
        {{--        url: "{{ route('admin.posts.translate-list') }}",--}}
        {{--        dataType: 'json',--}}
        {{--        data: function (params) {--}}
        {{--            return {lang: $('.tab-pane').data('lang'), q: params.term,chosen:$('.translate').val()};--}}
        {{--        },--}}
        {{--        processResults: function (data) {--}}
        {{--            return {--}}
        {{--                results: $.map(data.results, function (item) {--}}
        {{--                    return {--}}
        {{--                        text: item.title,--}}
        {{--                        id: item.id--}}
        {{--                    }--}}
        {{--                })--}}
        {{--            };--}}


        {{--        },--}}
        {{--    }--}}
        {{--});--}}

        {{--let _categoryTranslation = $('.translateCategory');--}}
        {{--if (_categoryTranslation.length) {--}}
        {{--    _categoryTranslation.select2({--}}
        {{--        tags: false,--}}
        {{--        allowClear: true,--}}
        {{--        minimumInputLength: 3,--}}
        {{--        ajax: {--}}
        {{--            type: 'GET',--}}
        {{--            url: "{{ route('admin.categories.translate-list') }}",--}}
        {{--            dataType: 'json',--}}
        {{--            data: function (params) {--}}
        {{--                return {lang: $('.tab-pane').data('lang'), q: params.term,chosen:_categoryTranslation.val()};--}}
        {{--            },--}}
        {{--            processResults: function (data) {--}}
        {{--                return {--}}
        {{--                    results: $.map(data.results, function (item) {--}}
        {{--                        return {--}}
        {{--                            text: item.title,--}}
        {{--                            id: item.id--}}
        {{--                        }--}}
        {{--                    })--}}
        {{--                };--}}


        {{--            },--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}
    })
</script>

@yield('additional_scripts')
@yield('additional_partial_scripts')
