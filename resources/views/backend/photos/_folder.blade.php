<a class="folder" data-id="{{ $folder->id }}">
    <img src="{{ asset('images/folder.png') }}" alt="{{ $folder->title }}">
    <span class="folder-title">{{ $folder->title }}</span>
</a>
