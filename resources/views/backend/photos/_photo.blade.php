<div class="files-list-item"
     data-id="{{ $photo->id }}"
     data-filename="{{ $photo->filename }}"
     data-src="{{ get_image($photo->filename, $photo->path,'list') }}"
     data-datetime="{{ $photo->created_at }}"
>
    <div class="file">
        <img src="{{ get_image($photo->filename, $photo->path, 'list') }}" data-filename="{{ $photo->filename }}" data-path="{{ $photo->path }}" height="100" alt="" class="img-default" title="{{$photo->author->name ? $photo->author->name . " - tərəfindən əlavə edilib" : ''}}">
    </div>
</div>
