@extends('backend.layouts.app')

@section('title', $post->title)

@section('content')
    <div>
        <form action="{{ route('admin.posts.update', $post->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" value="{{ $post->getTranslation('title', $lang) }}" name="title[{{ $lang }}]" id="title-{{ $lang }}" class="title form-control @error('title.' . $lang) is-invalid @enderror">

                                            @error('title.' . $lang)
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="slug-{{ $lang }}">@lang('admin.slug')</label>
                                            <input type="text" name="slug[{{ $lang }}]" id="slug-{{ $lang }}" class="form-control @error('slug.' . $lang) is-invalid @enderror" value="{{ $post->slug }}">

                                            @error('slug.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="info-{{ $lang }}">@lang('admin.info')</label>
                                            <textarea name="info[{{ $lang }}]" id="info-{{ $lang }}" class="form-control" rows="4">{!! $post->getTranslation('info', $lang) !!}</textarea>

                                            @error('info.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description[{{ $lang }}]" id="description-{{ $lang }}" class="ckeditor form-control" rows="4">
                                                 {!! $post->getTranslation('description', $lang) !!}
                                            </textarea>

                                            @error('description.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                @endforeach


                                <div class="form-group">
                                    @widget('PhotoManager\PhotoManager', ['photos' => $photos])

                                    @error('photos')
                                    <span class="text-danger">{{ $message}}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input
                                    value="{{ $post->image ? asset('storage/posts/' . $post->image) : null }}"
                                    name="image" id="image" type="file"
                                    class="file_uploader  @error('image') is-invalid @enderror"
                                    data-delete="{{ route('admin.posts.image-delete', $post->id) }}">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="background">@lang('admin.background_image')</label>
                                <input
                                    value="{{ $post->background ? asset('storage/posts/' . $post->background) : null }}"
                                    name="background" id="background" type="file"
                                    class="file_uploader  @error('background') is-invalid @enderror"
                                    data-delete="{{ route('admin.posts.background-delete', $post->id) }}">
                                @error('background')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if($post->status == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
