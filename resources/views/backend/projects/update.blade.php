@extends('backend.layouts.app')

@section('title', $project->title)

@section('content')
    <div>
        <form action="{{ route('admin.projects.update', $project->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" value="{{ $project->getTranslation('title', $lang) }}" name="title[{{ $lang }}]" id="title-{{ $lang }}" class="title autoGenerateSlug form-control @error('title.' . $lang) is-invalid @enderror">

                                            @error('title.' . $lang)
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="slug-{{ $lang }}">@lang('admin.slug')</label>
                                            <input type="text" name="slug[{{ $lang }}]" id="slug-{{ $lang }}" class="slug generatedSlug form-control @error('slug.' . $lang) is-invalid @enderror" value="{{ $project->slug }}">

                                            @error('slug.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description[{{ $lang }}]" id="description-{{ $lang }}" class="ckeditor form-control" rows="4">
                                                 {!! $project->getTranslation('description', $lang) !!}
                                            </textarea>

                                            @error('description.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                @endforeach

                                <div class="form-group">
                                    @widget('PhotoManager\PhotoManager', ['photos' => $photos])

                                    @error('photos')
                                    <span class="text-danger">{{ $message}}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="category">@lang('admin.category')</label>
                                <select name="category_id" id="category"
                                        class="form-control @error('category_id') is-invalid @enderror select2"
                                        data-placeholder="@lang('admin.select_category')">
                                    <option value="" selected>@lang('admin.select_category')</option>
                                    @foreach($categories as $category)
                                        @if(old('category'))
                                            <option
                                                value="{{ $category->id }}" {{$category->id == old('category') ? 'selected' :''}}>{{ $category->title }}</option>
                                        @else
                                            <option
                                                value="{{ $category->id }}" {{$category->id == $project->category_id ? 'selected' :''}}>{{ $category->title }}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @error('category_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input
                                    value="{{ $project->image ? asset('storage/projects/' . $project->image) : null }}"
                                    name="image" id="image" type="file"
                                    class="file_uploader  @error('image') is-invalid @enderror"
                                    data-delete="{{ route('admin.projects.image-delete', $project->id) }}">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if($project->status == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
