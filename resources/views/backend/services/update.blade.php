@extends('backend.layouts.app')

@section('title', $service->title)

@section('content')
    <div>
        <form action="{{ route('admin.services.update', $service->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <li class="nav-item">
                                        <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                    <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" value="{{ $service->getTranslation('title', $lang) }}" name="title[{{ $lang }}]" id="title-{{ $lang }}" class="title form-control @error('title.' . $lang) is-invalid @enderror">

                                            @error('title.' . $lang)
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="slug-{{ $lang }}">@lang('admin.slug')</label>
                                            <input type="text" name="slug[{{ $lang }}]" id="slug-{{ $lang }}" class="form-control @error('slug.' . $lang) is-invalid @enderror" value="{{ $service->slug }}">

                                            @error('slug.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description[{{ $lang }}]" id="description-{{ $lang }}" class="ckeditor form-control" rows="4">
                                                 {!! $service->getTranslation('description', $lang) !!}
                                            </textarea>

                                            @error('description.' . $lang)
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="form-group">
                                @widget('PhotoManager\PhotoManager', ['photos' => $photos])

                                @error('photos')
                                <span class="text-danger">{{ $message}}</span>
                                @enderror
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="image">@lang('admin.background_image')</label>
                                <input
                                    value="{{ $service->image ? asset('storage/services/' . $service->image) : null }}"
                                    name="image" id="image" type="file"
                                    class="file_uploader  @error('image') is-invalid @enderror"
                                    data-delete="{{ route('admin.services.image-delete', $service->id) }}">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="icon">@lang('admin.icon')</label>
                                <input
                                    value="{{ $service->icon ? asset('storage/services/' . $service->icon) : null }}"
                                    name="icon" id="icon" type="file"
                                    class="file_uploader  @error('icon') is-invalid @enderror"
                                    data-delete="{{ route('admin.services.icon-delete', $service->id) }}">
                                @error('icon')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if($service->status == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
