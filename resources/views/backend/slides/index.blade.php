@extends('backend.layouts.app')

@section('title', trans('admin.slides'))

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
            'columnFields' => [
                [
                    'format' => 'html',
                    'value' => function (\App\Models\Slide $model) {
                        return '<a title="' . trans('admin.move_up') . '" href="' . route('admin.slides.move', ['id' => $model->id, 'type' => 'up']) . '"><span class="fa fa-arrow-up"></span></a> ' .
                                '<a title="' . trans('admin.move_down') . '" href="' . route('admin.slides.move', ['id' => $model->id, 'type' => 'down']) . '"><span class="fa fa-arrow-down"></span></a>';
                    },
                    'filter' => false,
                    'htmlAttributes' => ['style' => 'width:100px']
                ],
                [
                    'label' => trans('admin.title'),
                    'attribute' => 'title',
                    'value' => function (\App\Models\Slide $model) {
                        return \App\Helpers\NodeHelper::titleWithPrefix($model->title, $model->depth);
                    }
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\Status::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\Status::getList()
                    ]
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.slides.destroy', $data->id);
                        }
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
