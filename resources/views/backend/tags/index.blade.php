@extends('backend.layouts.app')

@section('title', trans('admin.tags'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'gridHeader' => view('backend.tags._partial')->render(),
                'columnFields' => [
                [
                    'label' => trans('admin.name'),
                    'attribute' => 'name'
                ],
                [
                    'label' => trans('admin.language'),
                    'attribute' => 'language'
                ],
                [
                    'label' => trans('admin.count'),
                    'attribute' => 'count'
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit'
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
