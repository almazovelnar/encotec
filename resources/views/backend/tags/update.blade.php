@extends('backend.layouts.app')

@section('title', $tag->name)

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.tag.update', $tag->id) }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="name">@lang('admin.name')</label>
                                    <input type="text" value="{{ $tag->name }}" name="name" id="name" class="form-control @error('name') is-invalid @enderror">
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="slug">@lang('admin.slug')</label>
                                    <input type="text" value="{{ $tag->slug }}" name="slug" id="slug" class="form-control @error('slug') is-invalid @enderror">
                                    @error('slug')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="description">@lang('admin.description')</label>
                                    <textarea name="description" id="description" class="ckEditor form-control @error('description') is-invalid @enderror" rows="4">{!! $tag->description !!}</textarea>

                                    @error('description')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>

                    <div class="col-md-4">
                        <div class="card card-secondary">
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group">
                                        <div class="float-right">
                                            <label for="chosen">@lang('admin.chosen')</label>
                                            <label class="switch switch-success">
                                                <input type="checkbox" name="chosen" id="chosen"
                                                       @if($tag->chosen==1) checked @endif>
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="col-12">
                                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
