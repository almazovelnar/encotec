@extends('backend.layouts.app')

@section('title', trans('admin.translate_title', ['title' => $translate->group . '.' . $translate->key]))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.translates.update', $translate->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-8">

                        <div class="card card-primary card-outline card-tabs">
                            <div class="card-header p-0 pt-1 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">

                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                                            <div class="form-group">
                                                <label for="text-{{ $lang }}">@lang('admin.translation')</label>
                                                <input type="text" id="text-{{ $lang }}" class="form-control @error('text.' . $lang) is-invalid @enderror" name="text[{{ $lang }}]" value="{{ $translate->getTranslation($lang) }}">
                                                @error('text.' . $lang)
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="card card-primary card-tabs">


                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    <div class="form-group">
                                        <label for="group">@lang('admin.translate_group')</label>
                                        <input type="text" id="group" class="form-control @error('group') is-invalid @enderror" name="group" value="{{ $translate->group }}">
                                        @error('group')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="key">@lang('admin.translate_key')</label>
                                        <input type="text" id="name" class="form-control @error('key') is-invalid @enderror" name="key" value="{{ $translate->key }}">
                                        @error('key')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>


                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
