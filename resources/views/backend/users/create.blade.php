@extends('backend.layouts.app')

@section('title', trans('admin.new_user'))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.users.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="email">@lang('admin.email')</label>
                                            <input type="text" id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="name">@lang('admin.name')</label>
                                            <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="password">@lang('admin.password')</label>
                                            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="repassword">@lang('admin.repeat_password')</label>
                                            <input type="password" id="repassword" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation">
                                            @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-md-4">
                        <div class="card card-secondary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="role">@lang('admin.user_role')</label>
                                    <select id="role" class="form-control custom-select" name="role">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if(old('role')==$role->id) selected @endif>{{ trans('admin.role.' . $role->name) }}</option>
                                        @endforeach
                                    </select>
                                    @error('role')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="thumb">@lang('admin.thumb')</label>
                                    <input name="thumb" id="input-id" type="file" class="file_uploader">
                                    @error('thumb')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <br>

                                <div class="form-group">
                                    <div class="float-right">
                                        <label for="status">@lang('admin.status.status')</label>
                                        <label class="switch switch-success">
                                            <input type="checkbox" name="status" id="status" @if(old('status') == 1) checked @endif>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <input type="submit" name="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
