@extends('backend.layouts.app')

@section('title', trans('admin.user'))

@section('content')
<div class="card">
    <div class="card-header">
        <h3>{{ $user->name }}</h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="text-muted">
                    <p><strong>ID: </strong>{{ $user->id }}</p>
                </div>

                <div class="text-muted">
                    <p><strong>@lang('admin.email'):</strong></p>
                    {{ $user->email }}
                </div>
                <br>

                <div class="text-muted">
                    <p><strong>@lang('admin.created_at'):</strong></p>
                    {{ $user->created_at }}
                </div>
                <br>

                <div class="text-muted">
                    <p><strong>@lang('admin.status.status')</strong></p>
                    {{ $user->status ? trans('admin.status.active') : trans('admin.status.deactive') }}
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection
