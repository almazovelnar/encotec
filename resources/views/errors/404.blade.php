@extends('frontend.layouts.app')

@section('title', trans('site.page.404_not_found'))

@section('content')
    <!-- 404 not found -->
    <section class="not-found">
        <div class="container-fluid">
            <h1><b>404</b> <br> {!! editable()->text('site.layout.404_not_found') !!}</h1>
        </div>
    </section>
    <!-- 404 not found -->
@endsection


