@extends('frontend.layouts.app')

@section('title', trans('site.page.about'))

@section('content')
    <section class="about">
            <div class="cover" style="background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ $page->image ? asset('storage/pages/' . $page->image) : '/assets/frontend/images/about-cover.jpg' }})">
            <div class="cover__text container">
                <h1>{{ $page->title }}</h1>

                <div class="breadcrumbs-block">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{!! editable()->text('site.layout.home_page') !!}</a></li>
                        <li>{{ $page->title }}</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="wrapper">
            <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt="{{ site_info('title') }}"></div>
            <div class="sections">
                <div class="container">
                    <div class="about__main">
                        <div class="about__main__image">
                            {!! editable()->img('about_image_1') !!}
                            <div class="about__main__stats">
                                <p>{{ site_config('EXPERIENCE_YEARS_COUNT') }} <i>+</i></p>
                                <span>{!! editable()->text('site.statistics.year_of_experience') !!}</span>
                            </div>
                        </div>
                        <div class="section-head text-start">
                            <span class="text-red">{!! editable()->text('site.about.about_us_section_subtitle') !!}</span>
                            <h2 class="section-title text-black">{!! editable()->text('site.about.about_us_section_title') !!}</h2>
                        </div>
                        <div class="about__main__text">{!! $page->description !!}</div>
                    </div>
                    <div class="about__who">
                        <div class="section-head">
                            <span>{!! editable()->text('site.about.who_we_are_section_subtitle') !!}</span>
                            <h2 class="section-title">{!! editable()->text('site.about.who_we_are_section_title') !!}</h2>
                        </div>
                        <div class="about__who__content">
                            <h3>{!! editable()->text('site.about.who_we_are_title') !!}</h3>
                            {!! editable()->img('about_image_2') !!}
                            <p>{!! editable()->text('site.about.who_we_are_text') !!}</p>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                @if(!$clients->isEmpty())
                    <section class="partners">
                        <div class="container">
                            <div class="section-head">
                                <span>{!! editable()->text('site.about.clients_subtitle') !!}</span>
                                <h2 class="section-title">{!! editable()->text('site.about.clients_title') !!}</h2>
                            </div>
                            <div class="partners__slider"
                                 data-flickity='{ "cellAlign": "left", "contain": true, "imagesLoaded": true, "wrapAround": true, "pageDots": false, "autoPlay": 3500 }'>
                                @foreach($clients as $client)
                                    <div class="partners__item"><a href="{{ $client->link }}" target="_blank"><img src="{{ asset('storage/partners/' . $client->image) }}" alt="{{ $client->title }}"></a></div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                @endif

                <div class="container">
                    @if(!$certificates->isEmpty())
                        <div class="about__certificates">
                            <div class="section-head">
                                <span>{!! editable()->text('site.about.certificates_subtitle') !!}</span>
                                <h2 class="section-title">{!! editable()->text('site.about.certificates_subtitle') !!}</h2>
                            </div>
                            <div class="row"
                                 data-flickity='{ "imagesLoaded": true,"pageDots":false,"prevNextButtons":false,"wrapAround":true,"autoPlay":4000,"selectedAttraction": 0.02 }'>
                                @foreach($certificates as $certificate)
                                    <div class="col-md-4 col-12">
                                        <a href="{{ asset('storage/certificates/' . $certificate->file) }}" data-fancybox class="about__certificates__item">
                                            <div class="about__certificates__image">
                                                <img src="{{ asset('storage/certificates/' . $certificate->image) }}" alt="{{ $certificate->title }}">
                                            </div>
                                            <div class="about__certificates__text">
                                                <p>{{ $certificate->title }}</p>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    @if(!$reports->isEmpty())
                        <div class="about__reports">
                            <div class="section-head">
                                <span>{!! editable()->text('site.about.reports_subtitle') !!}</span>
                                <h2 class="section-title">{!! editable()->text('site.about.reports_subtitle') !!}</h2>
                            </div>
                            <div class="row justify-content-center">
                                @foreach($reports as $report)
                                    <div class="col-md-2 col-6">
                                        <a href="{{ asset('storage/reports/' . $report->file) }}" class="about__reports__item">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="11.5rem" height="12.8rem">
                                                <use xlink:href="#svg-pdf"></use>
                                            </svg>
                                            {{ $report->title }}
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

                @include('frontend.partials.career')
            </div>
        </div>
    </section>
@endsection
