@extends('frontend.layouts.app')

@section('content')
    @if(!$slides->isEmpty())
        <section class="banner">
            <div class="banner__slider"
                 data-flickity='{ "imagesLoaded": true,"pageDots":false,"prevNextButtons":false,"wrapAround":true,"autoPlay":5000,"selectedAttraction": 0.02 }'>
                @foreach($slides as $slide)
                    <div class="banner__item">
                        <div class="banner__image"><img src="{{ asset('storage/slides/' . $slide->image) }}" alt="{{ $slide->title }}"></div>
                        <div class="container">
                            <div class="banner__text">
                                <h2>{{ $slide->title }}</h2>
                                <p>{!! $slide->description !!}</p>
                                @if($slide->link)
                                    <div class="banner__btn">
                                        <a href="{{ $slide->link }}" class="btn" target="_blank">
                                            {!! editable()->text('site.layout.learn_more') !!}
                                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="8">
                                                <use xlink:href="#svg-arrow-right"></use>
                                            </svg>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    @endif

    <div class="wrapper">
        <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt="{{ site_info('title') }}"></div>
        <div class="sections">
            @include('frontend.partials._statistics')

            <!-- About section -->
            <section class="about-section">
                <div class="container">
                    <div class="about-section__row">
                        <div class="about-section__text">
                            <div class="section-head text-start">
                                <span>{!! editable()->text('site.home.about_us_section_subtitle') !!}</span>
                                <h2 class="section-title">{!! editable()->text('site.home.about_us_section_title') !!}</h2>
                            </div>
                            <div class="about-section__desc">
                                <h3>{!! editable()->text('site.home.about_us_section_short_text') !!}</h3>
                                <p>{!! editable()->text('site.home.about_us_section_text') !!}</p>
                            </div>
                            <a href="{{ route('about') }}" class="btn">
                                {!! editable()->text('site.home.about_learn_more') !!}
                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="8">
                                    <use xlink:href="#svg-arrow-right"></use>
                                </svg>
                            </a>
                        </div>
                        <div class="about-section__images">
                            <div class="about-section__images__item top">
                                {!! editable()->img('home_about_image_1', trans('site.home.about_us_section_title')) !!}
                            </div>
                            <div class="about-section__images__item bottom">
                                {!! editable()->img('home_about_image_2', trans('site.home.about_us_section_title')) !!}
                            </div>
                            <div class="play-btn video-popup" data-video-id="{{ trans('site.about.youtube_video_id') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="34">
                                    <use xlink:href="#svg-play"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End About section -->

            <!-- Who we are -->
            <section class="who">
                <div class="container">
                    <div class="section-head">
                        <span>{!! editable()->text('site.home.about_us_section_subtitle_2') !!}</span>
                        <h2 class="section-title">{!! editable()->text('site.home.about_us_section_title_2') !!}</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-6">
                            <a href="{{ route('about') }}" class="who__item">
                                <span class="who__icon"><img src="/assets/frontend/images/who1.svg" alt="@lang('site.home.about_us')"></span>
                                <h3>{!! editable()->text('site.home.about_us') !!}</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{ route('about') }}" class="who__item">
                                <span class="who__icon"><img src="/assets/frontend/images/who2.svg" alt="@lang('site.home.clients')"></span>
                                <h3>{!! editable()->text('site.home.clients') !!}</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{ route('about') }}" class="who__item">
                                <span class="who__icon"><img src="/assets/frontend/images/who3.svg" alt="@lang('site.home.certificates')"></span>
                                <h3>{!! editable()->text('site.home.certificates') !!}</h3>
                            </a>
                        </div>
                        <div class="col-md-3 col-6">
                            <a href="{{ route('about') }}" class="who__item">
                                <span class="who__icon"><img src="/assets/frontend/images/who4.svg" alt="@lang('site.home.reports')"></span>
                                <h3>{!! editable()->text('site.home.reports') !!}</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Who we are -->

            @if(!$services->isEmpty())
                <!-- What we do -->
                <section class="services-section">
                    <div class="container">
                        <div class="section-head">
                            <span>{!! editable()->text('site.home.services_section_subtitle') !!}</span>
                            <h2 class="section-title">{!! editable()->text('site.home.services_section_title') !!}</h2>
                        </div>
                        <div class="row">
                            @foreach($services as $service)
                                <div class="col-md-4 col-6">
                                    <a href="{{ route('service', $service->slug) }}" class="services-section__item">
                                        <span class="services-section__icon">
                                            <img src="{{ asset('storage/services/' . $service->icon) }}" alt="{{ $service->title }}">
                                        </span>
                                        <h3>{{ $service->title }}</h3>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
                <!-- End What we do -->
            @endif

            @if(!$projects->isEmpty())
                <!-- Our Works -->
                    <section class="works">
                        <div class="container">
                            <div class="section-head">
                                <span>{!! editable()->text('site.home.projects_section_subtitle') !!}</span>
                                <h2 class="section-title">{!! editable()->text('site.home.projects_section_title') !!}</h2>
                            </div>
                            <div class="works__row">
                                @foreach($projects as $project)
                                    <a href="{{ route('project', $project->slug) }}" class="works__item getProject">
                                        <div class="works__image"><img src="{{ asset('storage/projects/' . $project->image) }}" alt="{{ $project->title }}"></div>
                                        <div class="works__text">
                                            <p>{{ $project->category_title }}</p>
                                            <h3>{{ $project->title }}</h3>
                                            <span>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9" height="10">
                                                    <use xlink:href="#svg-plus"></use>
                                                </svg>
                                            </span>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                </section>
                <!-- End Our Works -->
            @endif

            @if(!$clients->isEmpty())
                <!-- Partners -->
                <section class="partners">
                    <div class="container">
                        <div class="section-head">
                            <span>{!! editable()->text('site.home.clients_section_subtitle') !!}</span>
                            <h2 class="section-title">{!! editable()->text('site.home.clients_section_title') !!}</h2>
                        </div>
                        <div class="partners__slider"
                             data-flickity='{ "cellAlign": "left", "contain": true, "imagesLoaded": true, "wrapAround": true, "pageDots": false, "autoPlay": 3500 }'>
                            @foreach($clients as $client)
                                <div class="partners__item">
                                    <a href="{{ $client->link }}" target="_blank">
                                        <img src="{{ asset('storage/partners/' . $client->image) }}" alt="{{ $client->title }}">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
                <!-- End Partners -->
            @endif

            @if(!$posts->isEmpty())
                <!-- News -->
                <section class="news-section">
                    <div class="container-fluid p-0">
                        <div class="section-head">
                            <span>{!! editable()->text('site.home.news_section_subtitle') !!}</span>
                            <h2 class="section-title">{!! editable()->text('site.home.news_section_title') !!}</h2>
                        </div>
                        <div class="news-section__row">
                            @foreach($posts as $key => $post)
                                <a href="{{ route('post', $post->slug) }}" @class(['news-section__item', 'active' => ($key==0) ])>
                                    <div class="news-section__image">
                                        <img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}">
                                    </div>
                                    <div class="news-section__text">
                                        <h3>{{ $post->title }}</h3>
                                        <p>{{ $post->info }}</p>
                                        <span class="news-section__link">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="18">
                                                <use xlink:href="#svg-arrow-right"></use>
                                            </svg>
                                        </span>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </section>
                <!-- End News -->
            @endif

            @include('frontend.partials.career')
        </div>
    </div>

    <div class="popup">
        <div class="popup__blur"></div>
        <button type="button" class="popup__close">
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18">
                <use xlink:href="#svg-close"></use>
            </svg>
        </button>
        <div class="popup__body"></div>
    </div>
@endsection
