<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(isset($title))
        <title>{{ $title }}</title>
    @else
        <title>@yield('title')</title>
    @endif

    {!! MetaTag::tag('description') !!}
    {!! MetaTag::tag('keywords') !!}
    {!! MetaTag::tag('og:title') !!}
    {!! MetaTag::tag('og:description') !!}
    {!! MetaTag::tag('og:image') !!}

    <meta name="twitter:card" content="summary_large_image">
    <link rel="shortcut icon" href="{{ get_favicon() }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include('frontend.layouts.styles')
    <script src="{{ mix('assets/frontend/js/svg-symbols.js') }}"></script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    @include('frontend.layouts.header')

    <div id="all_parts">
        @yield('content')
    </div>

    @include('frontend.layouts.footer')

    @include('frontend.layouts.scripts')
    @include('frontend.layouts.editable')
</body>
</html>
