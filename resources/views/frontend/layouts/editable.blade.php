<script>
    $('.editable').each(function () {
        if ($(this).css('position') === 'static') {
            $(this).css('position', 'relative');
        }
    })
    let clicks = 0;
    $(document).on('contextmenu', '.editable', function (e) {
        e.preventDefault();
        clicks++;
        if(clicks === 2){
            let url = $(this).data('edit-url');
            if (url.length) {
                window.open(url, '_blank').focus();
            }
        }

        if(clicks===1) setTimeout(function(){ clicks=0; }, 1000);
    })
</script>
<style>
    .editable {
        z-index: 99999999!important;
        cursor: pointer!important;
        display: inherit!important;
        font-size: inherit!important;
        color: inherit!important;
        margin-bottom: inherit!important;
        text-transform: inherit!important;
        font-weight: inherit!important;
        line-height: inherit!important;
    }
    .editable-image {
        border: 3px dashed #ff00fb!important;
        user-select: none!important;
    }
    .editable-text {
        border-bottom: 1px dashed #ff00fb!important;
    }
</style>
