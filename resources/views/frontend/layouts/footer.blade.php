<!-- Contacts section -->
<section class="contacts-section" id="contacts">
    @if(!empty(site_config('MAP_IFRAME')))
        <div id="map">
            {!! site_config('MAP_IFRAME') !!}
        </div>
    @endif
    <div class="container">
        <div class="contacts-section__info">
            <div class="contacts-section__address">
                <span>{!! editable()->text('site.layout.company_address') !!}</span>
                <p>{{ site_info('address') }}</p>
            </div>
            <ul>
                <li>
                    <span>{!! editable()->text('site.layout.company_phones') !!}</span>
                    @foreach(site_info('phone') as $phone)
                        <a href="tel:{{ $phone }}">{{ $phone }}</a>
                    @endforeach
                </li>
                <li>
                    <span>{!! editable()->text('site.layout.company_emails') !!}</span>
                    @foreach(site_info('email') as $email)
                        <a href="mailto:{{ $email }}">{{ $email }}</a>
                    @endforeach
                </li>
            </ul>
        </div>
    </div>
</section>
<!-- End Contacts section -->

<!-- footer -->
<footer class="footer">
    <div class="container">
        <div class="footer__left">
            <a href="{{ route('home') }}" class="footer__logo" aria-label="logo">
                <img src="/assets/frontend/images/logo-footer.png" width="128" height="20" alt="{{site_info('title')}}">
            </a>
            @if(site_config('REQUEST_PHONE'))
                <a class="footer__num">{{ site_config('REQUEST_PHONE') }}</a>
            @endif
            <ul class="footer__socials">
                @if(site_config('TWITTER'))
                    <li>
                        <a href="{{ site_config('TWITTER') }}" aria-label="twitter">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="16">
                                <use xlink:href="#svg-twitter"></use>
                            </svg>
                        </a>
                    </li>
                @endif
                @if(site_config('FACEBOOK'))
                    <li>
                        <a href="{{ site_config('FACEBOOK') }}" aria-label="facebook">
                            <svg xmlns="http://www.w3.org/2000/svg" width="11" height="19">
                                <use xlink:href="#svg-fb"></use>
                            </svg>
                        </a>
                    </li>
                @endif
                @if(site_config('LINKEDIN'))
                    <li>
                        <a href="{{ site_config('LINKEDIN') }}" aria-label="linkedin">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16">
                                <use xlink:href="#svg-linkedin"></use>
                            </svg>
                        </a>
                    </li>
                @endif
                @if(site_config('YOUTUBE'))
                    <li>
                        <a href="{{ site_config('YOUTUBE') }}" aria-label="youtube">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14">
                                <use xlink:href="#svg-youtube"></use>
                            </svg>
                        </a>
                    </li>
                @endif
            </ul>
            <p>{!! editable()->text('site.layout.footer_text') !!}</p>
        </div>
        <div class="footer__right">
            <div class="footer__menu">
                @widget('menu-footer')
            </div>
            <p class="copy">
                Developed by <a href="https://digitello.agency" target="_blank">Digitello Agency</a> for <strong>Encotec JSC</strong>
                {!! editable()->text('site.layout.all_rights_reserved', ['year' => '2022']) !!}
            </p>
        </div>
    </div>
</footer>
<!-- end footer -->
