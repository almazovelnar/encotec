<div class="svg-placeholder" style="border: 0; clip: rect(0 0 0 0); height: 1px;
        margin: -1px; overflow: hidden; padding: 0;
        position: absolute; width: 1px;"></div>
<script>
    document
        .querySelector('.svg-placeholder')
        .innerHTML = SVG_SPRITE;
</script>

<header class="header">
    <div class="container">
        <input type="checkbox" id="burger_toggle" class="d-none">
        <a href="{{ route('home') }}" class="header__logo" aria-label="logo">
            <img src="/assets/frontend/images/encotec.png" width="200" height="33" alt="{{ site_info('title') }}">
        </a>

        @widget('menu-header')
        @widget('lang-switcher')

        <label for="burger_toggle" class="header__burger d-lg-none"><i></i></label>
    </div>
</header>
