@php
    /**
     * @var \Illuminate\Pagination\LengthAwarePaginator $paginator
     * @var array[] $elements
     */
@endphp
@if ($paginator->hasPages())
    <ul class="pagination">
            @if (!$paginator->onFirstPage())
                    <li>
                        <a href="{{ $paginator->previousPageUrl() }}" class="pagination__arrow pagination__arrow--prev">
                            <img src="/assets/frontend/images/svg-icons/page-prev.svg" width="18" height="20" alt="prev">
                            <span>{!! editable()->text('site.layout.prev_page') !!}</span>
                        </a>
                    </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach($elements as $key => $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li><a class="pagination__num active">{{ $page }}</a></li>
                        @else
                            <li><a class="pagination__num" href="{{ $paginator->url($page) }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                    {{-- "Three Dots" Separator --}}
                @elseif(is_string($element))
                    <a class="pagination__num disabled">{{ $element }}</a>
                @endif
            @endforeach


            @if ($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}" class="pagination__arrow pagination__arrow--next">
                        <span>{!! editable()->text('site.layout.next_page') !!}</span>
                        <img src="/assets/frontend/images/svg-icons/page-next.svg" width="18" height="20" alt="next">
                    </a>
                </li>
            @endif
    </ul>
@endif
