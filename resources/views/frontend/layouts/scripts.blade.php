<script src="{{ mix('assets/frontend/js/app.js') }}"></script>

<script>
    $('.getProject').on('click', function (e) {
        e.preventDefault();
        let that = $(this);

        $('.popup .popup__body').html('').css('visibility', 'hidden');

        $.ajax({
            url: that.attr('href'),
            dataType: 'json',
            success: function (res) {
                if (res.success) {
                    $('.popup .popup__body').html(res.html)
                    $('.popup__slider').flickity();
                    setTimeout(function () {
                        $('.popup .popup__body').css('visibility', 'visible');
                    }, 100);
                }
            }
        })
    })
</script>

@yield('scripts')
@yield('footer-scripts')
