<link rel="stylesheet" href="{{ mix('assets/frontend/css/app.css') }}">
<link rel="stylesheet" href="{{ mix('assets/frontend/css/font/stylesheet.css') }}">

@yield('styles')
