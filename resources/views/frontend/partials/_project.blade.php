<div class="popup__slider"
     data-flickity='{ "imagesLoaded": true,"prevNextButtons":false,"wrapAround":true,"selectedAttraction": 0.02 }'>
    @foreach($project->photos as $photo)
        <div class="popup__slider__item">
            <img src='{{ get_image($photo->filename, $photo->path) }}' alt="{{ $project->title }}">
        </div>
    @endforeach
</div>
<div class="popup__desc">
    <h3>{{ $project->title }}</h3>
    <div class="info">
        {!! $project->description !!}
    </div>
</div>
