@if(!empty(site_config('COMPLETED_PROJECTS_COUNT')) &&
!empty(site_config('PERSONAL_COUNT')) &&
!empty(site_config('EXPERIENCE_YEARS_COUNT')) &&
!empty(site_config('CUSTOMERS_COUNT')))
    <!-- Statistics -->
    <section class="statistics">
        <div class="container">
            <div class="row g-0" id="counters_1">
                <div class="col-md-3 col-6">
                    <div class="statistics__item">
                        <h2><span class="counter" data-TargetNum="{{ site_config('COMPLETED_PROJECTS_COUNT') }}" data-Speed="3000">0</span>+</h2>
                        <p>{!! editable()->text('site.statistics.completed_projects') !!}</p>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="statistics__item">
                        <h2><span class="counter" data-TargetNum="{{ site_config('PERSONAL_COUNT') }}" data-Speed="3000">0</span>+</h2>
                        <p>{!! editable()->text('site.statistics.personal') !!}</p>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="statistics__item">
                        <h2><span class="counter" data-TargetNum="{{ site_config('EXPERIENCE_YEARS_COUNT') }}" data-Speed="3000">0</span>+</h2>
                        <p>{!! editable()->text('site.statistics.year_of_experience') !!}</p>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="statistics__item">
                        <h2><span class="counter" data-TargetNum="{{ site_config('CUSTOMERS_COUNT') }}" data-Speed="3000">0</span>+</h2>
                        <p>{!! editable()->text('site.statistics.customers') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Statistics -->
@endif
