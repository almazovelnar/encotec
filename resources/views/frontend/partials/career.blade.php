<!-- Career section -->
<section class="career-section" id="career">
    <div class="container">
        <div class="section-head">
            <span>{!! editable()->text('site.layout.career_section_subtitle') !!}</span>
            <h2 class="section-title">{!! editable()->text('site.layout.career_section_title') !!}</h2>
        </div>
        <div class="career-section__row">
            <p>
                {!! editable()->text('site.layout.career_text', ['email' => "<a href='" . site_config('CAREER_EMAIL') ."'>" . site_config('CAREER_EMAIL') ."</a>"]) !!}
            </p>
            <div class="career-section__image">
                {!! editable()->img('career_image', trans('site.layout.career_section_title')) !!}
            </div>
        </div>
    </div>
</section>
<!-- End Career section -->
