@extends('frontend.layouts.app')

@section('content')
    <section class="news-inner">
        <div class="cover" style="background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ $post->background_image ? asset('storage/posts/' . $page->background_image) : '/assets/frontend/images/news-cover.jpg' }})">
            <div class="cover__text container">
                <h1>{{ $post->title }}</h1>

                <div class="breadcrumbs-block">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{!! editable()->text('site.layout.home_page') !!}</a></li>
                        <li><a href="{{ route($page->slug) }}">{{ $page->title }}</a></li>
                        <li>{{ $post->title }}</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="wrapper">
            <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt="{{ site_info('title') }}"></div>
            <div class="sections">
                <div class="container">
                    <div class="news-inner__image">
                        <img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}">
                    </div>
                    <div class="news-inner__content">
                        <h2>{{ $post->title }}</h2>
                        {!! $post->description !!}
                    </div>
                    <div class="row">
                        @foreach($post->photos as $photo)
                            <div class="col-md-6">
                                <img src="{{ get_image($photo->filename, $photo->path) }}" alt="{{ $post->title }}">
                            </div>
                        @endforeach
                    </div>
                </div>
                @include('frontend.partials.career')
            </div>
        </div>
    </section>
@endsection
