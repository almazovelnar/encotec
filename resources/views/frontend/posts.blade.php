@extends('frontend.layouts.app')

@section('content')
    <section class="news">
        <div class="cover" style="background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ $page->image ? asset('storage/pages/' . $page->image) : '/assets/frontend/images/news-cover.jpg' }})">
            <div class="cover__text container">
                <h1>{{ $page->title }}</h1>
                <div class="breadcrumbs-block">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{!! editable()->text('site.layout.home_page') !!}</a></li>
                        <li>{{ $page->title }}</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="wrapper">
            <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt="{{ site_info('title') }}"></div>
            <div class="sections">
                @if(!$posts->isEmpty())
                    <div class="container">
                        <div class="news__items">
                            @foreach($posts as $post)
                                <a href="{{ route('post', $post->slug) }}" class="news__item">
                                    <div class="news__image">
                                        <img src="{{ asset('storage/posts/' . $post->image) }}" alt="{{ $post->title }}">
                                    </div>
                                    <div class="news__text">
                                        <h2>{{ $post->title }}</h2>
                                        <p>{{ $post->info }}</p>
                                        <span class="btn btn--with_icon btn-white-transparent">
                                            {!! editable()->text('site.posts.learn_more') !!}
                                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="8">
                                                <use xlink:href="#svg-arrow-right"></use>
                                            </svg>
                                        </span>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        {{ $posts->links('frontend.layouts.pagination') }}
                    </div>
                @endif
                @include('frontend.partials.career')
            </div>
        </div>
    </section>
@endsection
