@extends('frontend.layouts.app')

@section('content')
    <section class="portfolio">
        <div class="cover" style="background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ $page->image ? asset('storage/pages/' . $page->image) : '/assets/frontend/images/portfolio-cover.jpg' }})">
            <div class="cover__text container">
                <h1>{{ $page->title }}</h1>

                <div class="breadcrumbs-block">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{!! editable()->text('site.layout.home_page') !!}</a></li>
                        <li>{{ $page->title }}</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="wrapper">
            <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt=""></div>
            <div class="sections">
                <div class="container">
                    <div class="portfolio__tab">
                        <a href="{{ route('projects') }}" class="@if(!$category) active @endif">{!! editable()->text('site.project.all') !!}</a>
                        @foreach($categories as $categoryOne)
                            <a class="@if($categoryOne->slug == $category?->slug) active @endif" href="{{ route('project-category', $categoryOne->slug) }}">{{ $categoryOne->title }}</a>
                        @endforeach
                    </div>
                    <div class="portfolio__text">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sed diam ac neque ullamcorper
                            commodo. Proin ante mauris, ultricies sit amet lectus nec, pretium fringilla dolor. In a
                            urna vel
                            neque feugiat varius. Etiam molestie accumsan ultrices. Duis gravida lacus magna, non porta
                            justo
                            eleifend quis. Donec rhoncus pellentesque iaculis. Curabitur ut iaculis felis. Maecenas et
                            velit
                            facilisis, tincidunt sapien sed, congue ipsum. Etiam leo libero, sagittis ut lacinia in,
                            convallis
                            eget ante. Proin vulputate eros nisi. Praesent id ligula dui.
                        </p>
                    </div>
                    <div class="row">
                        @foreach($projects as $project)
                            <div class="col-md-4">
                                <a href="{{ route('project', $project->slug) }}" class="works__item getProject">
                                    <div class="works__image">
                                        <img src="{{ asset('storage/projects/' . $project->image) }}" alt="{{ $project->title }}">
                                    </div>
                                    <div class="works__text">
                                        <p>{{ $project->category->title }}</p>
                                        <h3>{{ $project->title }}</h3>
                                        <span>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23">
                                            <use xlink:href="#svg-plus"></use>
                                        </svg>
                                    </span>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                @include('frontend.partials.career')
            </div>
        </div>
    </section>

    <div class="popup">
        <div class="popup__blur"></div>
        <button type="button" class="popup__close">
            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18">
                <use xlink:href="#svg-close"></use>
            </svg>
        </button>
        <div class="popup__body"></div>
    </div>
@endsection
