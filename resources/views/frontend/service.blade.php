@extends('frontend.layouts.app')

@section('content')
    <section class="service">
        <div class="cover"
             style="background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ $service->image ? asset('storage/services/' . $service->image) : '/assets/frontend/images/consulting-cover.jpg' }})">
            <div class="cover__text container">
                <h1>{{ $service->title }}</h1>

                <div class="breadcrumbs-block">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{!! editable()->text('site.layout.home_page') !!}</a></li>
                        <li><a href="{{ route($page->slug) }}">{{ $page->title }}</a></li>
                        <li>{{ $service->title }}</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="wrapper">
            <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt="{{ site_info('title') }}"></div>
            <div class="sections">
                <div class="container">
                    @if(!$service->photos->isEmpty())
                        <div class="row service__images">
                            @foreach($service->photos as $photo)
                                <div class="col-4">
                                    <div class="service__image"><img src="{{ get_image($photo->filename, $photo->path) }}" alt="{{ $service->title }}"></div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    <div class="service__content">
                        <div class="service__text">
                            <h2>{{ $service->title }}</h2>
                            {!! $service->description !!}
                        </div>
                        <div class="service__next">
                            <h2>{!! editable()->text('site.services.next_service') !!}</h2>
                            <p>{{ $nextService->title }}</p>
                            <a href="{{ route('service', $nextService->slug) }}" class="service__next__icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="3.7rem" height="1.9rem">
                                    <use xlink:href="#svg-next"></use>
                                </svg>
                            </a>
                        </div>
                    </div>

                        @if(!$otherServices->isEmpty())
                            <div class="service__others">
                                <h2>{!! editable()->text('site.services.other_services') !!}</h2>
                                <div class="row">
                                    @foreach($otherServices as $otherService)
                                        <div class="col-md-3 col-6">
                                            <a href="{{ route('service', $otherService->slug) }}" class="service__others__item">
                                                <span>{!! editable()->text('site.services.explore_service') !!}</span>
                                                <h3>{{ $otherService->title }}</h3>
                                                <p>
                                                    {!! editable()->text('site.services.read_more') !!}
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="31" height="11">
                                                        <use xlink:href="#svg-next2"></use>
                                                    </svg>
                                                </p>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                </div>
                @include('frontend.partials.career')
            </div>
        </div>
    </section>
@endsection
