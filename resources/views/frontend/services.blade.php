@extends('frontend.layouts.app')

@section('content')
    <section class="services">
        <div class="cover" style="background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ $page->image ? asset('storage/pages/' . $page->image) : '/assets/frontend/images/service-cover.jpg' }})">
            <div class="cover__text container">
                <h1>{{ $page->title }}</h1>

                <div class="breadcrumbs-block">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{!! editable()->text('site.layout.home_page') !!}</a></li>
                        <li>{{ $page->title }}</li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="wrapper">
            <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt="{{ site_info('title') }}"></div>
            <div class="sections">
                <section class="services-section">
                    <div class="container container--sm">
                        <div class="section-head">
                            <span>{!! editable()->text('site.services.subtitle') !!}</span>
                            <h2 class="section-title">{!! editable()->text('site.services.title') !!}</h2>
                        </div>
                        <div class="row">
                            @foreach($services as $service)
                                <div class="col-md-6 col-6">
                                    <a href="{{ route('service', $service->slug) }}" class="services-section__item">
                                        <span class="services-section__icon">
                                            <img src="{{ asset('storage/services/' . $service->icon) }}" alt="{{ $service->title }}">
                                        </span>
                                        <h3>{{ $service->title }}</h3>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
                @include('frontend.partials.career')
            </div>
        </div>
    </section>
@endsection
