@extends('frontend.layouts.app')

@section('title', $page->title)

@section('content')
    <section class="news-inner">
        <div class="cover" style="background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('/assets/frontend/images/news-cover.jpg')">
            <div class="cover__text container">
                <h1>{{ $page->title }}</h1>

                <div class="breadcrumbs-block">
                    <ul class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{!! editable()->text('site.layout.home_page') !!}</a></li>
                        <li>{{ $page->title }}</li>
                    </ul>
                </div>

            </div>
        </div>
        <div class="wrapper">
            <div class="logo-fixed"><img src="/assets/frontend/images/logo-fixed.png" alt="{{ site_info('title') }}"></div>
            <div class="sections">
                <div class="container">
                    <div class="news-inner__image">
                        <img src="{{ asset('storage/pages/' . $page->image) }}" alt="{{ $page->title }}">
                    </div>
                    <div class="news-inner__content">
                        <h2>{{ $page->title }}</h2>
                        {!! $page->description !!}
                    </div>
                </div>

                @include('frontend.partials.career')
            </div>
        </div>
    </section>
@endsection
