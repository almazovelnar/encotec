<a class="btn btn-info btn-sm" href="{!! $url !!}" @if(!empty($htmlAttributes)) {!! $htmlAttributes !!} @endif >
    <i class="fas fa-pencil-alt"></i>
</a>&nbsp;
