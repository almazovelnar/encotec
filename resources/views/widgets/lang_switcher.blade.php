@if(count($locales = LaravelLocalization::getSupportedLanguagesKeys()) > 1)
    <div class="header__lang">
        <div class="header__lang__selected">{{ strtoupper(app()->getLocale()) }}</div>
        <ul class="header__lang__list">
            @foreach($locales as $locale)
                @if(app()->getLocale() != $locale)
                    <li><a href="{{ LaravelLocalization::localizeURL(url()->current(), $locale) }}">{{ strtoupper($locale) }}</a></li>
                @endif
            @endforeach
        </ul>
    </div>
@endif
