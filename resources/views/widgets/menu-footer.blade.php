@if(!empty($pages))
    <div class="footer__item">
        <h3>{!! editable()->text('site.layout.quick_links') !!}</h3>
        <ul>
            @foreach($pages as $page)
                @if($page->template == \App\Enum\PageTemplate::MAIN)
                    <li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
                @else
                    <li><a href="{{ route('page', $page->slug) }}">{{ $page->title }}</a></li>
                @endif
            @endforeach
        </ul>
    </div>
@endif

@if(!$branches->isEmpty())
    @foreach($branches as $branch)
        <div class="footer__item footer__item--contacts">
            <h3>{{ $branch->title }}</h3>
            <p>{!! $branch->address !!}</p>
            @foreach($branch->phone as $phone)
                <a href="tel:{{ $phone }}">{{ $phone }}</a>
            @endforeach
            @foreach($branch->email as $email)
                <a href="mailto:{{ $email }}">{{ $email }}</a>
            @endforeach
        </div>
    @endforeach
@endif
