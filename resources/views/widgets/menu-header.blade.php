@if(!empty($pages))
    <nav class="header__nav">
        <ul class="header__menu">
            @foreach($pages as $page)
                @if($page->template == \App\Enum\PageTemplate::MAIN)
                    <li><a href="{{ url($page->slug) }}">{{ $page->title }}</a></li>
                @else
                    <li><a href="{{ route('page', $page->slug) }}">{{ $page->title }}</a></li>
                @endif
            @endforeach
            <li><a href="#career">{!! editable()->text('site.layout.career') !!}</a></li>
            <li><a href="#contacts">{!! editable()->text('site.layout.contacts') !!}</a></li>
        </ul>
    </nav>
@endif
