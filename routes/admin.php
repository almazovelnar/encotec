<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CkeditorController;
use App\Http\Controllers\Backend\{Auth\LoginController,
    BranchController,
    CertificateController,
    ConfigController,
    ImageController,
    MainInfoController,
    PageController,
    PartnerController,
    PhotoController,
    Post\PostController,
    Project\CategoryController,
    Project\ProjectController,
    ReportController,
    Service\ServiceController,
    SiteController,
    SlideController,
    TagController,
    TranslateController,
    UserController};

Route::middleware(['auth', 'preventBackHistory'])->group(function () {
    Route::get('/', [SiteController::class, 'index'])->name('home');
    Route::resource('slides', SlideController::class)->except(['show']);
    Route::get('slides/move/{id}/{type}', [SlideController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('slides.move');
    Route::post('slides/image-delete/{id}', [SlideController::class, 'imageDelete'])
        ->where('id', '[0-9]+')
        ->name('slides.image-delete');

    Route::resource('partners', PartnerController::class)->except(['show']);
    Route::resource('branches', BranchController::class)->except(['show']);
    Route::get('branches/move/{id}/{type}', [PageController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('branches.move');

    Route::post('partners/image-delete/{id}', [PartnerController::class, 'imageDelete'])
        ->where('id', '[0-9]+')
        ->name('partners.image-delete');

    Route::resource('reports', ReportController::class)->except(['show']);
    Route::post('reports/file-delete/{id}/{lang}', [ReportController::class, 'fileDelete'])
        ->where('id', '[0-9]+')
        ->name('reports.file-delete');

    Route::resource('certificates', CertificateController::class)->except(['show']);
    Route::post('certificates/image-delete/{id}', [CertificateController::class, 'imageDelete'])
        ->where('id', '[0-9]+')
        ->name('certificates.image-delete');
    Route::post('certificates/file-delete/{id}/{lang}', [CertificateController::class, 'fileDelete'])
        ->where('id', '[0-9]+')
        ->name('certificates.file-delete');

    Route::resource('services', ServiceController::class)->except(['show']);
    Route::get('services/move/{id}/{type}', [ServiceController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('services.move');
    Route::post('services/image-delete/{id}', [ServiceController::class, 'imageDelete'])
        ->where('id', '[0-9]+')
        ->name('services.image-delete');
    Route::post('services/icon-delete/{id}', [ServiceController::class, 'iconDelete'])
        ->where('id', '[0-9]+')
        ->name('services.icon-delete');

    Route::resource('project-categories', CategoryController::class)->except(['show']);
    Route::get('project-categories/move/{id}/{type}', [CategoryController::class, 'move'])
        ->where('type', 'up|down')
        ->where('id', '[0-9]+')
        ->name('project-categories.move');
    Route::post('project-categories/image-delete/{id}', [CategoryController::class, 'imageDelete'])
        ->where('id', '[0-9]+')
        ->name('project-categories.image-delete');

    Route::resource('projects', ProjectController::class)->except(['show']);
    Route::post('projects/image-delete/{id}', [ProjectController::class, 'imageDelete'])
        ->where('id', '[0-9]+')
        ->name('projects.image-delete');

    Route::middleware('can:tagManage')->group(function () {
        Route::get('tag/merge', [TagController::class, 'merge'])
            ->name('tag.merge');
        Route::resource('tag', TagController::class);
    });

    Route::middleware('can:postManage')->group(function () {
        Route::resource('posts', PostController::class);
        Route::get('posts/tag-list', [PostController::class, 'tagList'])
            ->name('posts.tag-list');
        Route::post('posts/image-delete/{id}', [PostController::class, 'imageDelete'])
            ->where('id', '[0-9]+')
            ->name('posts.image-delete');
        Route::post('posts/bg-delete/{id}', [PostController::class, 'backgroundDelete'])
            ->where('id', '[0-9]+')
            ->name('posts.background-delete');
    });

    Route::middleware('can:pageManage')->group(function () {
        Route::resource('pages', PageController::class)->except(['show']);
        Route::post('pages/image-delete/{id}', [PageController::class, 'imageDelete'])
            ->where('id', '[0-9]+')
            ->name('pages.image-delete');
        Route::get('pages/move/{id}/{type}', [PageController::class, 'move'])
            ->where('type', 'up|down')
            ->where('id', '[0-9]+')
            ->name('pages.move');
    });

    Route::middleware('can:userManage')->group(function () {
        Route::post('users/delete-thumb/{id}', [UserController::class, 'deleteThumb'])
            ->name('users.delete-image');
        Route::resource('users', UserController::class);
    });

    Route::middleware('can:mainInfoManage')->group(function () {
        Route::post('main-info/delete-logo/{id}/{lang}', [MainInfoController::class, 'deleteLogo'])
            ->name('main-info.delete-logo');
        Route::post('main-info/delete-favicon/{id}/{lang}', [MainInfoController::class, 'deleteFavicon'])
            ->name('main-info.delete-favicon');
        Route::get('main-info', [MainInfoController::class, 'index'])->name('main-info');
        Route::put('main-info', [MainInfoController::class, 'update'])->name('main-info.update');
    });

    Route::resource('config', ConfigController::class)->middleware('can:configManage')->except(['show']);
    Route::resource('translates', TranslateController::class)->middleware('can:translateManage')->except(['show']);
    Route::resource('images', ImageController::class);

    Route::post('ckeditor/image_upload', [CkeditorController::class, 'upload'])->name('ckeupload');
    Route::post('tinymce/addWidget', [\App\Http\Controllers\TinymceController::class, 'addWidget'])->name('tinymce.addWidget');

    Route::prefix('photos')->name('photos.')->group(function () {
        Route::get('index', [PhotoController::class, 'index'])->name('index');
        Route::get('folder', [PhotoController::class, 'folderIndex'])->name('folder');
        Route::get('trash', [PhotoController::class, 'trashIndex'])->name('trash');
        Route::get('restore', [PhotoController::class, 'restore'])->name('restore');
        Route::post('create', [PhotoController::class, 'create'])->name('create');
        Route::get('update', [PhotoController::class, 'update'])->name('update');
        Route::post('update', [PhotoController::class, 'update'])->name('update.submit');
        Route::post('size', [PhotoController::class, 'size'])->name('size.submit');
        Route::get('create-folder', [PhotoController::class, 'folderCreate'])->name('create-folder');
        Route::get('delete-folder', [PhotoController::class, 'folderDelete'])->name('delete-folder');
        Route::get('update-folder', [PhotoController::class, 'folderUpdate'])->name('update-folder');
        Route::post('update-folder', [PhotoController::class, 'folderUpdate'])->name('update-folder.submit');
        Route::post('create-folder', [PhotoController::class, 'folderCreate'])->name('create-folder.submit');
        Route::post('delete', [PhotoController::class, 'delete'])->name('delete');
        Route::post('soft-delete', [PhotoController::class, 'softDelete'])->name('softDelete');
        Route::post('crop', [PhotoController::class, 'crop'])->name('crop');
        Route::get('sort', [PhotoController::class, 'sort'])->name('sort');
        Route::post('render-gallery', [PhotoController::class, 'renderGallery'])->name('render-gallery');
        Route::post('get-size', [PhotoController::class, 'getSize'])->name('get-size');
    });
});

Route::middleware(['preventBackHistory'])->group(function () {
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login'])->name('login.login');
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
});
