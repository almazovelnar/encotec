<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix(LaravelLocalization::setLocale())->middleware(['localize', 'apiLocale'])->group(function () {
    Route::prefix('home')->name('home.')->group(function () {
        Route::get('stories', [\App\Http\Controllers\Api\HomeController::class, 'getStories'])->name('stories');
        Route::get('shorts', [\App\Http\Controllers\Api\HomeController::class, 'getShorts'])->name('shorts');
        Route::get('chosen-posts', [\App\Http\Controllers\Api\HomeController::class, 'getChosenPosts'])->name('chosenPosts');
        Route::get('chosen-categories', [\App\Http\Controllers\Api\HomeController::class, 'getChosenCategoriesWithPosts'])->name('getChosenCategoriesWithPosts');
    });
    Route::prefix('sidebar')->name('sidebar.')->group(function () {
        Route::get('languages', [\App\Http\Controllers\Api\SidebarController::class, 'getLanguages'])->name('languages');
        Route::get('categories', [\App\Http\Controllers\Api\SidebarController::class, 'getCategories'])->name('categories');
        Route::get('channels', [\App\Http\Controllers\Api\SidebarController::class, 'getChannels'])->name('channels');
    });
    Route::prefix('category')->name('category.')->group(function () {
        Route::get('{id}', [\App\Http\Controllers\Api\CategoryController::class, 'getCategory'])
            ->where('id', '[0-9]+')
            ->name('detail');

        Route::get('{id}/chosen-posts', [\App\Http\Controllers\Api\CategoryController::class, 'getChosenPosts'])->name('chosen-posts');
        Route::get('{id}/posts', [\App\Http\Controllers\Api\CategoryController::class, 'getLatestPosts'])->name('posts');
        Route::get('{id}/last-post', [\App\Http\Controllers\Api\CategoryController::class, 'getLastPost'])->name('last-post');
    });
    Route::prefix('post')->name('post.')->group(function () {
        Route::get('{id}', [\App\Http\Controllers\Api\PostController::class, 'details'])
            ->where('id', '[0-9]+')
            ->name('details');
    });
    Route::prefix('page')->name('page.')->group(function () {
        Route::get('{id}', [\App\Http\Controllers\Api\StaticController::class, 'details'])
            ->where('id', '[0-9]+')
            ->name('details');

        Route::post('contact-submit', [\App\Http\Controllers\Api\StaticController::class, 'contactSubmit'])
            ->name('contact-submit');
    });
    Route::prefix('live')->name('live.')->group(function () {
        Route::get('id', [\App\Http\Controllers\Api\LiveController::class, 'getId'])->name('id');
        Route::get('url', [\App\Http\Controllers\Api\LiveController::class, 'getUrl'])->name('url');
    });

    Route::get('tags', [\App\Http\Controllers\Api\HomeController::class, 'getTags'])->name('tags');
    Route::get('search', [\App\Http\Controllers\Api\SearchController::class, 'getPosts'])->name('search-posts');
});
