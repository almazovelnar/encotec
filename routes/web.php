<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\PostController;
use App\Http\Controllers\Frontend\SiteController;
use App\Http\Controllers\Frontend\ServiceController;
use App\Http\Controllers\Frontend\ProjectController;

Route::prefix(LaravelLocalization::setLocale())->middleware(['localize'])->group(function () {
    Route::get('/', [SiteController::class, 'index'])->name('home');
    Route::get('/about-us', [\App\Http\Controllers\Frontend\StaticController::class, 'about'])->name('about');
    Route::get('/page/{slug}', [\App\Http\Controllers\Frontend\StaticController::class, 'view'])
        ->where('slug', '[\w\-]+')
        ->name('page');

    Route::get('/services', [ServiceController::class, 'index'])->name('services');
    Route::get('/service/{slug}', [ServiceController::class, 'view'])
        ->where('slug', '[\w\-]+')
        ->name('service');

    Route::get('/posts', [PostController::class, 'index'])->name('posts');
    Route::get('/post/{slug}', [PostController::class, 'view'])
        ->where('slug', '[\w\-]+')
        ->name('post');

    Route::get('/projects', [ProjectController::class, 'index'])
        ->name('projects');
    Route::get('/projects/category/{category}', [ProjectController::class, 'index'])
        ->where('category', '[\w\-]+')
        ->name('project-category');
    Route::get('/project/{slug}', [ProjectController::class, 'view'])
        ->where('slug', '[\w\-]+')
        ->name('project');
});
